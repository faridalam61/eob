<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthManager
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next, $guard = null): Response
    {
        if (!auth()->check()) {
            return redirect()->route('login');
        } else {

            $authUser = auth()->user();

            if ($authUser->active != 1) {
                Auth::logout();
                return back()->withErrors('You account is not active.');
            }

            if (null == $authUser->verified_at)
                return redirect()->route('verify')->withErrors('You account is not verified yet.');
            if ($authUser->type == 'employer' && 0 == $authUser->branches->count())
                return redirect()->route('branch.assign')->with('warning', 'You need to select your branch first.');

            if (!null == $guard) {
                switch ($guard) {
                    case 'admin':
                        if ($authUser->type != 'admin')
                            return back()->withErrors('You have not permission to access.');
                        break;
                    case 'employer':
                        if ($authUser->type != 'employer')
                            return back()->withErrors('You have not permission to access.');
                        break;
                    case 'candidate':
                        if ($authUser->type != 'candidate')
                            return back()->withErrors('You have not permission to access.');
                        break;
                    default:
                        return back()->withErrors('User type is not authorized to access.');
                }
                return $next($request);
            }
            return $next($request);
        }
    }
}
