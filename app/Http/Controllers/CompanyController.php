<?php

namespace App\Http\Controllers;

use App\Mail\ForgetPassword;
use App\Mail\Invite;
use App\Models\Branch;
use App\Models\Company;
use App\Models\CompanyInvite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        View::share('title', 'List of Employees');
        $companies = Company::query();

        if ($request->name)
            $companies->where('name', 'like', "%$request->name%");

        $companies = $companies->latest()->paginate(20);
        return view('back.company.list', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function entity(Request $request, $code = null)
    {
        if($request->method() == 'POST' && !null == $code){

            $company = Company::where('code', $code)->first();
            if (!$company)
                return back()->withErrors('Company information not found to update company information.');

            $request->validate([
                'name' => 'string|required',
                'email' => 'email|required|unique:companies',
            ]);

            $data['name'] = $request->name;
            $data['info_url'] = $request->info_url;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['hr_contact'] = $request->hr_contact;
            $data['review'] = $request->review;

            try {
                $company->update($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Company information failed to update.');
            }

            return redirect()->route('panel.company.branch', $company->code)->with('success', 'Company updated successfully.');
        } elseif ($request->method() == 'POST'){
            $request->validate([
                'name' => 'string|required',
                'email' => 'email|required|unique:companies',
            ]);

            $data['name'] = $request->name;
            $data['info_url'] = $request->info_url;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['hr_contact'] = $request->hr_contact;
            $data['review'] = $request->review;

            try {
                $company = Company::create($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Company information failed to save.');
            }

            return redirect()->route('panel.company.branch', $company->code)->with('success', 'Company saved successfully. Now add location for this company');
        } else {
            View::share('title', (!null == $code?'Edit Company Information': 'Add New Company'));
            $company = Company::where('code', $code)->first();
            return view('back.company.form', compact('company'));
        }
    }

    public function branch(Request $request, $code)
    {
        $company = Company::where('code', $code)->first();
        if (!$company)
            return back()->withErrors('Company information not found to manage locations.');
        $branch = Branch::firstWhere('code', $request->branchCode);

        if ($request->method() == 'POST' && !null == $branch) {
            if (!$branch)
                return back()->withErrors('Branch information not found to update.');

            $request->validate([
                'location' => 'required|string'
            ]);

            try {
                $branch->update(['location' => $request->location]);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('New location failed to update.');
            }
            return back()->with('success', 'New location updated successfully.');
        } elseif ($request->method() == 'POST') {
            $request->validate([
                'location' => 'required|string'
            ]);

            try {
                Branch::create(['company_id' => $company->id, 'location' => $request->location]);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('New location failed to added.');
            }
            return back()->with('success', 'New location added successfully.');
        } elseif($request->method() == 'DELETE' && $branch){
            try {
                $branch->delete();
            } catch
            (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('New location failed to delete.');
            }
            return back()->with('success', 'New location deleted successfully.');
        } else {
            $branches = Branch::where('company_id', $company->id)->latest()->paginate(20);
            View::share('title', 'Location of ' . $company->name);
            return view('back.company.branch', compact('branches', 'company'));
        }
    }

    public function invite(Request $request, $code = null)
    {
        if ($request->method() == 'POST' && !null == $code) {
            $company = Company::firstWhere('code', $code);
            if (!$company)
                return back()->withErrors('Company information not found to send invitation.');

            $request->validate([
                'code' => 'required|numeric|unique:company_invites',
                'email' => 'email|nullable'
            ]);

            $data['code'] = $request->code;
            $data['company_id'] = $company->id;
            $data['send_to'] = $request->email;

            try {
                CompanyInvite::create($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Can not create new invitation');
            }
            if ($request->email)
                $this->sendInvitationEmail($data);

            return back()->with('success', 'Invitation with code ' . $data['code'] . ' is allow open to register.');
        } else {
            if($code){
                $company = Company::firstWhere('code', $code);
                if(!$company)
                    return back()->withErrors('No invitation record found for this company.');
                $invites = CompanyInvite::where('company_id', $company->id)->orderBy('id', 'desc')->paginate(20);
                View::share('title', 'Invitation history of '.$company->name);
                return view('back.company.invite_history', compact('company', 'invites'));
            } else {
            View::share('title', 'Invite employer to sign in');
            $companies = Company::openBranches();

            $companies = $companies->latest()->paginate(20);
            return view('back.company.invite', compact('companies'));
        }
        }
    }

    protected function sendInvitationEmail($data)
    {
        $mail = Mail::to($data['send_to'])->send(new Invite($data));

        if(!$mail)
            return back()->withInput()->withErrors('Email sending failed');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($code)
    {
        $company = Company::firstWhere('code', $code);
        if(!$company)
            return back()->withErrors('Company information not found to delete.');
        try{
            $company->delete();
        } catch (\PDOException $e){
            if(config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return back()->withErrors('Company failed to delete.');
        }
        return back()->with('success', 'Company deleted successfully.');
    }
}
