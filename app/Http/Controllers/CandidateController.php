<?php

namespace App\Http\Controllers;

use App\Models\CandidateCategory;
use App\Models\Category;
use App\Models\Employer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class CandidateController extends Controller
{
    public function index(Request $request)
    {
        View::share('title', 'List of candidates');
        $candidates = User::where('type', 'candidate');

        if ($request->name)
            $candidates->where('name', 'like', "%$request->name%");

        $candidates = $candidates->latest()->paginate(20);
        return view('back.candidate.list', compact('candidates'));
    }

    public function entity(Request $request, $code = null)
    {
        if ($request->method() == 'POST' && !null == $code) {
            $candidate = User::where('type', 'candidate')->where('code', $code)->first();
            if (!$candidate)
                return back()->withInput()->withErrors('Candidate information not found to update.');
            $request->validate([
                'email' => 'required|email|unique:users,email,'.$candidate->id,
                'first_name' => 'string|nullable',
                'last_name' => 'string|nullable'
            ]);

            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            if ($request->password)
                $data['password'] = Hash::make($request->password);

            try {
                $candidate->update($data);
                $this->categoryUpdate($candidate->id, $request->categories);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Failed to update user information.');
            }


            return back()->with('success', 'User information updated successful.');
        } elseif ($request->method() == 'POST') {

            $request->validate([
                'email' => 'required|email|unique:users',
                'first_name' => 'string|nullable',
                'last_name' => 'string|nullable',
                'password' => 'string|required_if:password_confirmation,<=,1|confirmed|min:6'
            ]);

            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['password'] = Hash::make($request->password);
            $data['type'] = 'candidate';


            try {
                $candidate = User::create($data);
                $this->categoryUpdate($candidate->id, $request->categories);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Failed to update user information.');
            }


            return back()->with('success', 'Candidate information updated successful.');

        } else {
            View::share('title', !null == $code ? 'Edit Candidate information ' : 'Add new candidate');
            $candidate = User::where('type', 'candidate')->where('code', $code)->first();
            $categories = Category::parentOnly()->get(['id', 'name', 'code']);
            return view('back.candidate.form', compact('candidate', 'categories'));
        }
    }

    protected function categoryUpdate($candidateId, $categoryCodes = null){
        if(is_array($categoryCodes)){
            $categoryIds = Category::whereIn('code', $categoryCodes)->pluck('id');

            try{
                CandidateCategory::where('candidate_id', $candidateId)->whereNotIn('category_id', $categoryIds)->delete();
            } catch (\PDOException $e){
                if(config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return false;
            }

            foreach($categoryIds as $categoryId){
                try{
                    CandidateCategory::create(['candidate_id' => $candidateId, 'category_id' => $categoryId]);
                } catch (\PDOException $e){
                    if(config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function destroy($code){
        $candidate = User::where('type', 'candidate')->where('code', $code)->first();
        if(!$candidate)
            return back()->withErrors('Candidate information not found to delete.');
        try{
            $candidate->delete();
        } catch (\PDOException $e){
            if(config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return back()->withErrors('Failed to delete user.');
        }
        return back()->with('success', 'User deleted successfully.');
    }
}
