<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\CandidateJob;
use App\Models\Category;
use App\Models\Company;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        View::share('title', 'Job list');
        $jobs = Job::query();
        if($request->company_id)
            $jobs->where('company_id', $request->company_id);
        if($request->branch_id)
            $jobs->where('branch_id', $request->branch_id);
        if($request->category_id)
            $jobs->where('category_id', $request->category_id);
        if($request->subcategory_id)
            $jobs->where('subcategory_id', $request->subcategory_id);

        $jobs = $jobs->orderBy('id', 'desc')->paginate(20);
        return view('back.job.list', compact('jobs'));
    }

    public function entity(Request $request, $code = null)
    {
        $my = Auth::user();
        if(!$my)
            return back()->withErrors('User type not correct.');

        if ($request->method() == 'POST' && $code) {
            $job = Job::firstWhere('code', $code);
            if (!$job)
                return back()->withInput()->withErrors('Job informaiton not found to update.');

            $request->validate([
                'title' => 'string|unique:jobs,title,'.$job->id
            ]);

            $data['title'] = $request->title;
            $data['subtitle'] = $request->subtitle;
            $data['company_id'] = $this->company($request->company_id, $request->company_url);
            $data['branch_id'] = $this->branch($request->branch_id, $data['company_id']);
            $data['category_id'] = $this->categoryPrepare($request->category_id);
            $data['subcategory_id'] = $this->subcategoryPrepare($request->subcategory_id, $data['category_id']);
            $data['info_url'] = $request->job_url;
            $data['type'] = $request->job_type;
            $data['salary_estimate'] = $request->salary_estimate;
            $data['shift'] = $request->shift;
            $data['benefit'] = $request->benefits;
            $data['description'] = $request->details;

            try {
                $job->update($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withErrors('Job information can not update.');
            }
            return back()->with('success', 'Job information updated successfully.');
        } elseif ($request->method() == 'POST') {

            $request->validate([
                'title' => 'string|unique:jobs'
            ]);

            $data['title'] = $request->title;
            $data['subtitle'] = $request->subtitle;
            $data['company_id'] = $this->company($request->company_id, $request->company_url);
            $data['branch_id'] = $this->branch($request->branch_id, $data['company_id']);
            $data['category_id'] = $this->categoryPrepare($request->category_id);
            $data['subcategory_id'] = $this->subcategoryPrepare($request->subcategory_id, $data['category_id']);
            $data['info_url'] = $request->job_url;
            $data['type'] = $request->job_type;
            $data['salary_estimate'] = $request->salary_estimate;
            $data['shift'] = $request->shift;
            $data['benefit'] = $request->benefits;
            $data['description'] = $request->details;

            try {
                Job::create($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withErrors('Job information can not submit.');
            }

            return back()->with('success', 'Sub information successfully submitted');
        } else {
            View::share('title', $code ? 'Edit job information' : 'Add new job');
            $job = Job::where('code', $code)->first();
            $companies = Company::query()->get();
            if($companies->count() == 1)
                View::share('branches', Branch::whereIn('company_id', $companies->pluck('id'))->get(['id', 'code', 'location']));
            $categories = Category::whereNull('parent_id')->get();
            if($job){
                View::share('branches', Branch::where('company_id', $job->company_id)->get(['id', 'location']));
                View::share('subcategories', Category::where('parent_id', $job->category_id)->get(['id', 'parent_id', 'name']));
            }

            return view('back.job.form', compact('job', 'companies', 'categories'));
        }
    }

    public function import(Request $request)
    {
        if ($request->method() == 'POST') {
            $request->validate([
                'document' => 'required|mimes:csv,txt'
            ]);

            $file = $request->file('document');
            $fileContents = file($file->getPathname());

            $fileHeader = ['created_at', 'title', 'job_url', 'subtitle', 'branch', 'company', 'company_url', 'review', 'description', 'type', 'salary', 'shift', 'benefit', 'category', 'subcategory', 'expire_at'];
            $tableHeader = ["\u{FEFF}Job Posted Date", "Job Title", "Job URL", "Job Subtitle", "Job Location", "Company Name", "Company URL", "Company Review Count", "Job Description", "Job Type", "Salary Estimate", "Job Shift", "Job Benefits", "Category Name", "Sub Category", "Last date of apply"];
            $requiredField = ['title', 'job_url', 'subtitle', 'description', 'company'];

            $imported = 0;
            foreach ($fileContents as $index => $line) {
                if ($index == 0) {
                    if (empty(array_diff(array_values(str_getcsv($line)), array_values($tableHeader))) && empty(array_diff(array_values($tableHeader), array_values(str_getcsv($line))))) {
                        continue;
                    } else {
                        return back()->withErrors('File is not properly formatted. Download sample file, fill-up content and try again.');
                    }
                } else {
                    $row = str_getcsv($line);
                    foreach ($fileHeader as $index => $key) {
                        $collection[$key] = $row[$index] ?? null;
                    }

                    $skip = false;
                    foreach($requiredField as $field){
                        if(null == $collection[$field] &&  empty($collection[$field])){
                            $skip = true;
                            break;
                        }
                    }
                    if($skip)
                        continue;

                    $data['title'] = $collection['title'];
                    $data['subtitle'] = $collection['subtitle'];
                    $data['info_url'] = $collection['job_url'];
                    $data['company_id'] = $collection['company'] ? $this->company($collection['company'], $collection['company_url'], $collection['review']) : null;
                    $data['branch_id'] = $collection['branch'] ? $this->branch($collection['branch'], $data['company_id']) : null;
                    $data['description'] = $collection['description'];
                    $data['type'] = $collection['type'];
                    $data['salary_estimate'] = $collection['salary'];
                    $data['shift'] = $collection['shift'];
                    $data['benefit'] = $collection['benefit'];
                    $data['category_id'] = $collection['category'] ? $this->categoryPrepare($collection['category']) : null;
                    $data['subcategory_id'] = $collection['subcategory'] ? $this->categoryPrepare($collection['subcategory'], $data['category_id']) : null;
                    $data['expire_at'] = $collection['expire_at']?$this->dateFormatter($collection['expire_at']) : Carbon::now()->addMonth();
                    $data['created_at'] = $this->dateFormatter($collection['created_at']) ?? Carbon::now();

                    try {
                        Job::create($data);
                    } catch (\PDOException $e) {
                        if (config('app.debug'))
                            dd($e->getMessage());
                        Log::error($e);
                        continue;
                    }
                    $imported += 1;
                }
            }
            return redirect()->route('panel.job.list')->with('success', $imported.' job is imported from CSV');
        } else {
            View::share('title', 'Import jobs from CSV');
            $companies = Company::all();
            $categories = Category::parentOnly()->get();
            return view('back.job.import', compact('companies', 'categories'));
        }
    }

    protected function dateFormatter($input)
    {
        if (str_contains($input, 'ago')) {
            $now = Carbon::now();
            return $now->copy()->sub(Carbon::parse($input)->diffInSeconds());
        } else {
            try {
                $carbonDate = Carbon::createFromFormat('n/j/y', $input);
            } catch (\Exception $e) {
                return false;
            }
            if ($carbonDate instanceof Carbon)
                return $carbonDate;
            else
                return false;
        }
    }


    protected function company($name, $url = null, $review = null)
    {
        if(is_numeric($name))
            return $name;

        $search['name'] = $name;

        $data['info_url'] = $url;
        $data['review'] = (int) $review;

        try {
            $company = Company::firstOrCreate($search, $data);
        } catch (\PDOException $e) {
            if (config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return false;
        }
        return $company->id;
    }

    protected function branch($name, $company_id = null)
    {
        if(is_numeric($name))
            return $name;

        try {
            $branch = Branch::firstOrCreate(['location' => $name], ['company_id' => $company_id]);
        } catch (\PDOException $e) {
            if (config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return false;
        }
        return $branch->id;
    }

    protected function categoryPrepare($name)
    {
        if(is_numeric($name))
            return $name;

        try {
            $category = Category::firstOrCreate(['name' => $name]);
        } catch (\PDOException $e) {
            if (config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return false;
        }
        return $category->id;
    }

    protected function subcategoryPrepare($name, $category_id = null)
    {
        if(is_numeric($name))
            return $name;

        try {
            $subcategory = Category::firstOrCreate(['name' => $name], ['parent_id' => $category_id]);
        } catch (\PDOException $e) {
            if (config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return false;
        }
        return $subcategory->id;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($code)
    {
        try {
                Job::where('code', $code)->first()->delete() ?? '';
        } catch (\PDOException $e) {
            if (config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return back()->withErrors('The job can not delete.');
        }
        return back()->with('success', 'Job successfully deleted');
    }

    public function apply($code)
    {
        $job = Job::firstWhere('code', $code);
        $myJob = CandidateJob::updateOrCreate(['candidate_id' => auth()->id(), 'job_id' => $job->id], ['applied_at' => Carbon::now()]);
        if(!$job)
            return back()->withErrors('This job is no longer here.');
        return redirect()->away($job->info_url);
    }

    public function saved(Request $request){
        if(Auth::user()->type !== 'candidate')
            return back()->withErrors('You have no permission to access this page.');
        View::share('title', 'Saved job list');
        $jobs = Job::whereIn('id', CandidateJob::where('candidate_id', auth()->id())->whereNotNull('saved_at')->pluck('job_id'))->latest()->paginate(20);
        return \view('back.job.list', compact('jobs'));
    }

    public function applied(Request $request){
        if(Auth::user()->type !== 'candidate')
            return back()->withErrors('You have no permission to access this page.');
        View::share('title', 'Applied job list');
        $jobs = Job::whereIn('id', CandidateJob::where('candidate_id', auth()->id())->whereNotNull('applied_at')->pluck('job_id'))->latest()->paginate(20);
        return \view('back.job.list', compact('jobs'));
    }

    public function show($code){
        $job = Job::firstWhere('code', $code);
        if(!$job)
            return back()->withErrors('Job can is not exist. Try another.');
        View::share('title', $job->title);
        return view('back.job.show', compact('job'));
    }
}
