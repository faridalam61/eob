<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\CandidateJob;
use App\Models\Category;
use App\Models\Company;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use function PHPUnit\Framework\isFalse;

class AjaxController extends Controller
{
    public function saveJob($code = null){
        if($code){
            $job = Job::firstWhere('code', $code);
            if(!$job)
                return false;
            $myJob = CandidateJob::where('candidate_id', auth()->id())->where('job_id', $job->id)->first();
            if($myJob && $myJob->saved_at){
                try{
                    $myJob->update(['saved_at' => null]);
                } catch (\PDOException $e){
                    if(config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return json_encode(['saved' => 1, 'message' => 'Failed to submit']);
                }
                return json_encode(['saved' => 0, 'message' => 'Removed from saved list.']);
            } else {
                try{
                    CandidateJob::updateOrCreate(['candidate_id' => auth()->id(), 'job_id' => $job->id], ['saved_at' => Carbon::now()]);
                } catch (\PDOException $e){
                    if(config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return json_encode(['saved' => 0, 'message' => 'Removed from saved list.']);
                }
                return json_encode(['saved' => 1, 'message' => 'Failed to submit']);
            }
        } else {
            return json_encode(['message' => 'Failed to locate job for change action.']);
        }
    }

    public function jobInfo($code){
        $job = Job::firstWhere('code', $code);
        if(!$job)
            return json_encode(['status' => 'failed', 'message' => 'Job information not found.']);

        $data['code'] = $job->code;
        $data['title'] = $job->title;
        $data['subtitle'] = $job->subtitle;
        $data['employer_code'] = $job->company->code??null;
        $data['employer_name'] = $job->company->name??null;
        $data['location_code'] = $job->branch->code??null;
        $data['location_name'] = $job->branch->location??null;
        $data['category_code'] = $job->category->code??null;
        $data['category_name'] = $job->category->name??null;
        $data['subcategory_code'] = $job->subcategory->code??null;
        $data['subcategory_name'] = $job->subcategory->name??null;
        $data['description'] = $job->description;
        $data['salary'] = $job->salary;
        $data['shift'] = $job->shift;
        $data['benefit'] = $job->benefit;
        $data['info_url'] = $job->info_url;
        $data['is_saved'] = $job->is_saved;
        $data['active'] = $job->active;
        $data['expire'] = !null == $job->expire? Carbon::parse($job->expire)->format('d M Y'):null;
        $data['apply_code'] = ($job->active)?($job->expire_at?(Carbon::parse($job->expire_at)->isPast()?2:1):1):0;

        $response['job'] = $data;
        $response['status'] = 'success';

        return json_encode($response);
    }

    public function branches($id){
        $company = Company::firstWhere('id', $id);
        if(!$company)
            return json_encode([]);
        return json_encode(Branch::where('company_id', $company->id)->pluck('location', 'id')->toArray());
    }

    public function categorySub($id){
        $category = Category::firstWhere('id', $id);
        if(!$category)
            return json_encode([]);
        return json_encode(Category::where('parent_id', $category->id)->pluck('name', 'id')->toArray());
    }


}
