<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class CategoryController extends Controller
{

    public function index(Request $request, $code = null)
    {
        if ($request->method() == 'POST') {

        } else {
            $parent = Category::where('code', $request->parent_id)->first();

            $categories = Category::query();
            if ($parent) {
                View::share('title', 'Sub Categories under ' . $parent->name);
                View::share('parentId', $parent->id);
                $categories->where('parent_id', $parent->id);
            } else {
                View::share('title', 'Job Categories');
                $categories->parentOnly();
            }
            $categories = $categories->paginate(20);
            return view('back.category.list', compact('categories'));
        }
    }

    public function entity(Request $request, $code = null)
    {

        $category = Category::where('code', $code)->first();

        if ($request->method() == 'POST' && !null == $code) {
            if (!$category)
                return back()->withErrors('Category information not found to update.');
            try {
                $category->update(['name' => $request->name]);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withErrors('Category failed to update.');
            }
            return redirect()->route('panel.category.list')->with('success', 'Category updated successfully.');
        } elseif ($request->method() == 'POST') {
            $request->validate([
                'name' => 'string|required'
            ]);

            $data['name'] = $request->name;
            if ($request->parentId)
                $data['parent_id'] = $request->parentId;

            try {
                Category::create($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withErrors('Category failed to update.');
            }
            return redirect()->route('panel.category.list')->with('success', 'Category updated successfully.');
        } elseif ($request->method() == 'DELETE' && $code) {

        } else {
            View::share('title', !null == $code ? 'Edit category' : 'Add new category');
            return view('back.category.form', compact('category'));
        }

    }

    public function destroy($code)
    {
        $category = Category::firstWhere('code', $code);
        if (!$category)
            return back()->withErrors('Category not found to delete. Reload page and try again.');
        try {
            $category->delete();
        } catch (\PDOException $e) {
            if (config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return back()->withErrors('Category deletion failed.');
        }
        return back()->with('success', 'Category successfully deleted.');
    }
}
