<?php

namespace App\Http\Controllers;

use App\Helper\Panel;
use App\Models\Branch;
use App\Models\CandidateCategory;
use App\Models\Category;
use App\Models\Company;
use App\Models\Job;
use App\Models\UserBranch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class PanelController extends Controller
{
    public $jobTypes = [
        'full-time' => 'Full Time',
        'part-time' => 'Part Time',
        'contact' => 'Contact'
    ];
    public function home(Request $request){
        View::share('title', 'Dashboard');
        if (Auth::check()) {
            $my = Auth::user();
            switch ($my->type) {
                case 'admin':

                    return view('back.page.admin');
                case 'employer':

                    return view('back.page.employer');
                case 'candidate':
                    $jobs = Job::where('active', 1)->where(function ($query) {
                        $query->whereNull('expire_at')->orWhere('expire_at', '>=', Carbon::now());
                    });
                    if ($my->job_types)
                        $jobs->whereIn('type', explode('|', $my->job_types));

                    if ($request->keyword || $request->location) {
                        if ($request->keyword ?? false)
                            $jobs->where('title', 'like', "%$request->keyword%")->orWhere('subtitle', 'like', "%$request->keyword%");
                        if ($request->location) {
                            $inputModify = str_replace(' ', ',', $request->location);
                            $locations = str_replace(',,', '|', $inputModify);
                            $locations = explode('|', $locations);

                            $companies = Company::where('name', 'REGEXP', $locations)->get(['id']);
                            $branches = Branch::where('location', 'REGEXP', $locations)->get();

                            if ($companies->count()) {
                                $jobs->whereIn('company_id', $companies->pluck('id')->toArray());
                            } elseif ($branches->count()) {
                                $jobs->whereIn('branch_id', $branches->pluck('id')->toArray());
                            }
                        }
                    } elseif ($request->employer) {
                        $company = Company::firstWhere('code', $request->employer);
                        if($company)
                            $jobs->where('company_id', $company->id);
                    } elseif ($request->location) {
                        $branch = Branch::firstWhere('code', $request->location);
                        if($branch)
                            $jobs->where('branch_id', $branch->id);
                    } elseif ($request->category) {
                        $category = Category::firstWhere('code', $request->category);
                        if($category)
                            $jobs->where('category_id', $category->id);
                    } elseif ($request->subcategory) {
                        $subcategory = Category::firstWhere('code', $request->subcategory);
                        if($subcategory)
                            $jobs->where('subcategory_id', $subcategory->id);
                    } elseif ($my->categories->count()) {
                        $jobs->whereIn('category_id', $my->categories->pluck('id'));
                    }

                    $jobs = $jobs->latest()->paginate(10);

                    return view('back.page.candidate', compact('jobs'));
                default:
                    return view('back.auth.login')->withErrors('User type undefined.');
            }
        } else {
            Auth::logout();
            return redirect()->route('login');
        }
    }

    public function profile(Request $request){
        $user = Auth::user();
        View::share('title', 'My profile');
        if ($user->type == 'candidate')
            View::share('jobTypes', $this->jobTypes);
        return view('back.profile.view', compact('user'));
    }

    public function profileModify(Request $request)
    {
        if ($request->method() == 'POST') {
            $user = Auth::user();

            $request->validate([
                'first_name' => 'string|required',
                'last_name' => 'string|required',
                'email' => 'email|required',
                'phone' => 'numeric|nullable',
            ]);

            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;

            if($user->type == 'employer' && $request->company){

                $company['name'] = $request->company['name'];
                $company['email'] = $request->company['email'];
                $company['phone'] = $request->company['phone'];
                $company['info_url'] = $request->company['website'];
                $company['hr_contact'] = $request->company['hr_contact'];

                try{
                    $user->company->update($company);
                } catch (\PDOException $e){
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return back()->withInput()->withErrors('Your profile failed to update.');
                }
            }

            try {
                Auth::user()->update($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Your profile failed to update.');
            }
            return redirect()->route('panel.profile')->with('success', 'Your profile updated successful.');
        } else {
            View::share('title', 'Update profile');
            $user = Auth::user();
            return view('back.profile.form', compact('user'));
        }
    }

    public function jobPreference(Request $request)
    {
        $user =  Auth::user();
        if(!$user)
            return back()->withErrors('Your are not eligible for this service.');

        if($request->method() == 'POST'){
            $request->validate([
                'categories' => 'nullable|array'
            ]);

            $untaggedId = array();
            $categories = Category::whereIn('code', $request->categories??[])->get();

            foreach($categories as $category){
                try{
                    CandidateCategory::firstOrCreate(['candidate_id' => $user->id, 'category_id' => $category->id]);
                } catch (\PDOException $e) {
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return back()->withInput()->withErrors('Your selection can be updated');
                }
                $untaggedId[] = $category->id;
            }

            try{
                CandidateCategory::where('candidate_id', $user->id)->whereNotIn('category_id', $untaggedId)->delete();
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Existing category removal failed');
            }

            try {
                $user->update(['job_types' => is_array($request->job_types) ? implode('|', $request->job_types) : null]);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Failed to update preferred job types');
            }

            return redirect()->route('panel.profile')->with('success', 'Preferred categories updated.');
        } else {
            View::share('title', 'Job preference');
            $categories = Category::whereNull('parent_id')->orderBy('name')->get(['id', 'code', 'name']);
            $myCategories = Auth::user()->categories->pluck('id')->toArray()??[];
            $types = $this->jobTypes;
            return view('back.profile.job_preference', compact('categories', 'types', 'user', 'myCategories'));
        }
    }

    public function myBranch(Request $request){
        $user =  Auth::user();
        if($user->type != 'employer')
            return back()->withErrors('You are not eligible for this service.');

        if($request->method() == 'POST'){
            $request->validate([
                'branches' => 'nullable|array'
            ]);

            $find['user_id'] = $user->id;
            $update['company_id'] = $user->company_id;

            foreach ($request->branches as $branchId => $action) {

                $find['branch_id'] = $branchId;

                try {
                    if ((int) $action) {
                        UserBranch::firstOrCreate($find, $update);
                    } else {
                        UserBranch::where($find)->delete();
                    }
                } catch (\PDOException $e) {
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                }
            }

            return redirect()->route('panel.profile')->with('success', 'Location updated to your account.');
        } else {
            View::share('title', 'Select location under you.');
            $branches = Branch::where('company_id', $user->company_id)->orderBy('location')->get(['id', 'code', 'location']);
            $myBranchIds = Auth::user()->branches->pluck('id')->toArray()??[];
            return view('back.profile.my_branch', compact('branches', 'myBranchIds'));
        }
    }


    public function changePassword(Request $request)
    {
        if ($request->method() == 'POST') {

            $request->validate([
                'password' => 'required|confirmed|min:6',
            ]);

            $password = Hash::make($request->password);

            try {
                Auth::user()->update(['password' => $password]);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Failed to update your password');
            }
            return redirect()->route('panel.profile')->with('success', 'Your password updated successful.');
        } else {
            View::share('title', 'Update password');
            return view('back.profile.password');
        }
    }

    public function setting(Request $request)
    {
        if ($request->method() == 'POST') {
            foreach ($request->input() as $key => $value) {
                Panel::infoSet($key, $value);
            }
            $oldFile = false;
            foreach ($request->files as $key => $file) {
                if (Panel::infoSet($key))
                    $oldFile = Panel::infoSet($key);
                $fileName = $key . '.' . $request->file($key)->getClientOriginalExtension();
                Panel::infoSet($key, $fileName);
                if($oldFile)
                    Storage::disk('public')->delete($oldFile);
                Storage::disk('public')->putFileAs('/', $request->file($key), $fileName);
                $oldFile = false;
            }
            return back()->with('success', 'System setting successfully updated.');

        } else {
            View::share('title', 'System Setting');
            return view('back.setting.system');
        }
    }
}
