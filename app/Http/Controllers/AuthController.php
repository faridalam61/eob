<?php

namespace App\Http\Controllers;

use App\Mail\Verify;
use App\Models\CompanyInvite;
use App\Models\User;
use App\Models\UserBranch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{

    public function login(Request $request, $socialite = null)
    {
        if (Auth::check())
            return redirect()->route('panel.home')->with('success', 'You are already logged in.');

        if ($request->method() == 'POST') {
            $request->validate([
                'email' => 'string|required',
                'password' => 'required|min:6',
            ]);
            if (config('app.debug')) {
                $user = User::firstWhere('email', $request->email);
                if ($user && Auth::loginUsingId($user->id)) {
                    return redirect()->route('panel.home')->with('success', 'User already logged in .');
                } else {
                    try {
                        User::updateOrCreate(['email' => $request->email], ['password' => Hash::make($request->password), 'type' => 'admin', 'active' => 1, 'verified_at' => Carbon::now()]);
                    } catch (\PDOException $e) {
                        dd($e->getMessage());
                    }
                }
            }

            if (Auth::check())
                return redirect()->route('panel.home')->with('success', 'User already logged in .');

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1], $request->remember_me)) {
                return redirect()->route('panel.home');
            } else {
                return back()->withInput()->withErrors('Login credential is not correct. Try using correct mail and password.');
            }

        } else {
            View::share('title', 'Login');
            return view('back.auth.login');
        }
    }

    public function authCall($platform)
    {
        if (!in_array($platform, ['google', 'facebook']))
            return back()->withErrors('We are currently not supporting ' . $platform);
        return Socialite::driver($platform)->redirect();
    }

    public function authCallback($platform)
    {

        if (!in_array($platform, ['google', 'facebook']))
            return back()->withErrors('We are currently not supporting ' . $platform);

        try {
            $cloudUser = Socialite::driver($platform)->user();
        } catch (\Exception $e) {
            if (config('app.debug'))
                dd($e->getMessage());
            Log::error($e);
            return redirect()->route('login')->withErrors(ucfirst($platform) . ' login is in trouble. Contact administrator.');
        }

        if (auth()->check()) {
            $my = Auth::user();
            if (User::where($platform . '_uuid', $cloudUser->id)->where('id', '!=', $my->id)->first()) {
                return redirect()->route('panel.profile')->withErrors('This account is already attached with other user.');
            } else {
                try {
                    $my->update([$platform . '_uuid' => $cloudUser->id]);
                } catch (\PDOException $e) {
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return back()->withInput()->with('Failed to attach ' . $platform . ' as social login.');
                }

                return redirect()->route('panel.profile')->with('success', 'You ' . $platform . ' account is linked successfully.');
            }
        } elseif ($user = User::where($platform . '_uuid', $cloudUser->id)->first()) {
            Auth::loginUsingId($user->id);
            return redirect()->route('panel.home');
        } else {

            $data['first_name'] = $cloudUser->name;
            $data['email'] = $cloudUser->email;
            $data['verified'] = $platform;
            $data['verified_at'] = Carbon::now();
            $data['type'] = 'candidate';

            try {
                $user = User::create($data);
            } catch (\PDOException $e) {
                if (config('app.debug'))
                    dd($e->getMessage());
                Log::error($e);
                return back()->withInput()->withErrors('Employer registration failed.');
            }

            Auth::loginUsingId($user->id);
            return redirect()->route('panel.jop-preference')->with('You register here using ' . $platform);
        }
    }

    public function register(Request $request, $param = null)
    {
        if (Auth::check())
            return redirect()->route('panel.home')->with('success', 'You are already logged in.');

        if ($param == '-company') {

            if ($request->code) {
                $invite = CompanyInvite::firstWhere('code', $request->code);
                if (!$invite)
                    return back()->withInput()->withErrors('Your invitation code not correct');
                $company = $invite->company;
                if (!$company)
                    return back()->withInput()->withErrors('Employer information not found.');
            }

            if ($request->method() == 'POST') {
                if ($invite->activate_at)
                    return back()->withInput()->withErrors('This invitation is already completed');
                if (User::firstWhere('company_id', $invite->company_id))
                    return redirect()->route('login')->withInput()->withErrors('You are already registered. just login now.');

                $request->validate([
                    'email' => 'required|email|unique:users',
                    'password' => 'required|confirmed|min:6'
                ]);

                $data['first_name'] = $request->first_name;
                $data['last_name'] = $request->last_name;
                $data['email'] = $request->email;
                $data['phone'] = $request->phone;
                $data['password'] = Hash::make($request->password);
                $data['company_id'] = $invite->company_id;
                $data['verified'] = 'email';
                $data['verified_at'] = Carbon::now();
                $data['type'] = 'employer';

                try {
                    $user = User::create($data);
                } catch (\PDOException $e) {
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return back()->withInput()->with('Can not add user information with company.');
                }

                try {
                    $invite->update(['activate_at' => Carbon::now()]);
                } catch (\PDOException $e) {
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                }

                if (Auth::loginUsingId($user->id))
                    return redirect()->route('branch.assign');
                else
                    return redirect()->route('login')->with('success', 'Your registration completed. Login now.');

            } else {
                if ($request->code) {
                    $invite = CompanyInvite::firstWhere('code', $request->code);
                    if (!$invite)
                        return redirect()->route('register', '-company')->withErrors('Invalid invitation code. Request for another code and try again.');
                    if ($invite->activate_at)
                        return redirect()->route('login')->withErrors('You have already complete this process. Just login now.');
                    if (User::firstWhere('company_id', $invite->company_id))
                        return redirect()->route('login')->withInput()->withErrors('You are already registered. just login now.');

                    try {
                        $invite->update(['clicked_at' => Carbon::now()]);
                    } catch (\PDOException $e) {
                        if (config('app.debug'))
                            dd($e->getMessage());
                        Log::error($e);
                    }

                    $code = $request->code;
                    View::share('title', 'Employer Registration');
                    $company->email = $invite->send_to ?? $company->email;

                    return view('back.auth.register-company', compact('code', 'company'));
                } else {
                    View::share('title', 'Employer Registration');
                    return view('back.auth.register-company');
                }
            }
        } else {

            if ($request->method() == 'POST') {
                $request->validate([
                    'email' => 'email|required|unique:users',
                    'password' => 'required|confirmed|min:6'
                ]);

                $data['first_name'] = $request->first_name;
                $data['last_name'] = $request->last_name;
                $data['email'] = $request->email;
                $data['phone'] = $request->phone;
                $data['password'] = Hash::make($request->password);
                $data['type'] = 'candidate';

                try {
                    $user = User::create($data);
                } catch (\PDOException $e) {
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                    return back()->withInput()->withErrors('Employer registration failed.');
                }

                if (Mail::to($request->email)->send(new Verify($user->id))) {
                    return redirect()->route('verify')->with('success', 'A verification email already send to you. Verify and explore more.');
                } else {
                    return redirect()->route('login')->with('success', 'You are registered now. Login to access the system.');
                }

            } else {
                View::share('title', 'Employee registration');
                return view('back.auth.register');
            }
        }
    }

    public function branchAssign(Request $request)
    {
        if (!Auth::check())
            return redirect()->route('login');

        if ($request->method() == 'POST') {

            $request->validate([
                'branch' => 'array|required'
            ]);

            $my = Auth::user();
            $find['user_id'] = $my->id;
            $find['company_id'] = $my->company_id;

            foreach ($request->branch as $branchId => $action) {

                $create['branch_id'] = $branchId;
                try {
                    if ($action)
                        UserBranch::firstOrCreate($find, $create);
                    else
                        UserBranch::where($find)->delete();

                } catch (\PDOException $e) {
                    if (config('app.debug'))
                        dd($e->getMessage());
                    Log::error($e);
                }
            }

            return redirect()->route('panel.home')->with('success', 'No new branch able to assign. Explore more.');
        } else {
            $my = Auth::user();
            $company = $my->company;
            if (!$company)
                return redirect()->route('panel.home')->with('success', 'Your registration completed. Login now.');
            $branchEngaged = \App\Models\UserBranch::where('company_id', $company->id)->pluck('id')->toArray() ?? [];
            $branches = \App\Models\Branch::where('company_id', $company->id)->whereNotIn('id', $branchEngaged)->get();
            $myBranchIds = $my->branches->pluck('id')->toArray();
            View::share('title', 'Select your handled areas');
            return view('back.auth.branch', compact('branches', 'myBranchIds'));
        }
    }

    public function recover(Request $request)
    {
        if (Auth::check())
            return redirect()->route('panel.home')->with('success', 'You are already logged in.');


    }

    public function verify(Request $request, $code = null)
    {
        if (!Auth::check())
            return redirect()->route('login')->withErrors('Logged in first. Only logged in user can verify.');

        if ($request->method() == 'POST') {
            $my = Auth::user();
            if (Mail::to($my->email)->send(new Verify($my->id))) {
                return redirect()->route('verify')->with('success', 'A verification email already send to you. Verify and explore more.');
            } else {
                return back()->withErrors('Not able to send verification email due to technical issue.');
            }

        } elseif ($request->method() == 'GET' && !null == $code) {
            $data = explode('|', base64_decode($code));
            $userCode = $data[0] ?? null;
            $date = $data[1] ?? null;
            if (!$date || !$userCode)
                return redirect()->route('login')->withErrors('Verification link is not valid');
            $user = User::firstWhere('code', $userCode);
            if (!$user)
                return redirect()->route('login')->withErrors('Verification link is not valid to identify user.');
            if ($user->verified_at)
                return redirect()->route('login')->withErrors('You are already verified. Just login here.');
            if (Carbon::parse($date)->endOfDay()->isFuture()) {
                try {
                    $user->update(['verified' => 'email', 'verified_at' => Carbon::now()]);
                } catch (\PDOException $e) {

                }
                Auth::loginUsingId($user->id);
                if (auth()->user()->type == 'candidate')
                    return redirect()->route('panel.jop-preference')->with('success', 'You are successfully verified and logged in.');
                else
                    return redirect()->route('panel.home')->with('success', 'You are successfully verified and logged in.');
            } else {
                return redirect()->route('login')->withErrors('Your verification link is no no longer active. Login and request for another verification email.');
            }
        } else {
            View::share('title', 'Verify your email now.');
            return view('back.auth.verify');
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return back()->with('success', 'You are logged out successfully.');
    }
}
