<?php

namespace App\Models;

use App\Models\Scopes\MyJobs;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Job extends Model
{
    use HasFactory,SoftDeletes;

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model['code'] = uniqid();
            $model['post_date'] = $model->post_date??Carbon::now();
        });
    }

    protected static function booted(): void
    {
        static::addGlobalScope(new MyJobs);
    }

    public function getIsSavedAttribute(){
        if(auth()->user()->type != 'candidate')
            return false;
        $myJob = CandidateJob::where('candidate_id', Auth::id())->where('job_id', $this->id)->first();
        return ($myJob && !null == $myJob->saved_at)?true:false;
    }
    public function getIsAppliedAttribute(){
        if(auth()->user()->type != 'candidate')
            return false;
        $myJob = CandidateJob::where('candidate_id', Auth::id())->where('job_id', $this->id)->first();
        return ($myJob && !null == $myJob->applied_at)?true:false;
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function branch(){
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function subcategory(){
        return $this->belongsTo(Category::class, 'subcategory_id', 'id')->whereNotNull('parent_id');
    }
}
