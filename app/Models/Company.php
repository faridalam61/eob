<?php

namespace App\Models;

use App\Models\Scopes\MyCompany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpParser\Builder;

class Company extends Model
{
    use HasFactory,SoftDeletes;

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model['code'] = uniqid();
        });
    }

    protected static function booted(): void
    {
        static::addGlobalScope(new MyCompany);
    }

    public function branches(){
        return $this->hasMany(Branch::class, 'company_id', 'id');
    }

    public function jobs(){
        return $this->hasMany(Job::class, 'company_id', 'id');
    }

    public function user(){
        return $this->hasOne(User::class, 'company_id', 'id');
    }

    public function invites(){
        return $this->hasMany(CompanyInvite::class, 'company_id', 'id');
    }

    public function scopeOpenBranches($query)
    {
        return $query->whereHas('branches', function ($subQuery) {
            $subQuery->whereDoesntHave('booking');
        });
    }
}
