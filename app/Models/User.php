<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model['code'] = uniqid();
        });
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getNameAttribute()
    {
        $thisName = array();
        foreach (['first_name', 'last_name'] as $namePart) {
            if (!null == $this->$namePart)
                $thisName[] = $this->$namePart;
        }
        if (count($thisName))
            return implode(' ', $thisName);
        else
            return $this->email;
    }

    public function applied_jobs(){ //'id', 'candidate_id', 'applied_id', 'id'
        return $this->hasManyThrough(Job::class, CandidateJob::class, 'candidate_id', 'id', 'id', 'job_id')->whereNotNull('applied_at');
    }

    public function saved_jobs(){ //'id', 'candidate_id', 'saved_id', 'id'
        return $this->hasManyThrough(Job::class, CandidateJob::class, 'candidate_id', 'id', 'id', 'job_id')->whereNotNull('saved_at');
    }

    public function categories(){// 'id', 'candidate_id', 'category_id', 'id'
        return $this->hasManyThrough(Category::class, CandidateCategory::class, 'candidate_id', 'id', 'id', 'category_id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function branches(){ // 'id', 'usr_id', 'branch_id', 'id;
        return $this->hasManyThrough(Branch::class, UserBranch::class, 'user_id', 'id', 'id', 'branch_id');
    }
}
