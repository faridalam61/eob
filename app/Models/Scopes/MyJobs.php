<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MyJobs implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if(auth()->check()){
            $my = auth()->user();
            if($my->type == 'employer'){
                if($my->branches->count()??false)
                    $builder->where('company_id', $my->company_id)->whereIn('branch_id', $my->branches->pluck('id'));
                else
                    $builder->where('company_id', $my->company_id);
            }
        }
    }
}
