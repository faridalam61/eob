<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use HasFactory,SoftDeletes;

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model['code'] = uniqid();
        });
    }

    public function scopeAssigned($query){
        return $query->whereHas('booking');
    }
    public function scopeFree($query){
        return $query->whereDoesntHave('booking');
    }

    public function scopeUnbooked($query){
        return $query->whereHasNo('booking');
    }

    public function jobs(){
        return $this->hasMany(Job::class, 'branch_id', 'id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function booking(){
        return $this->hasOne(UserBranch::class, 'branch_id', 'id');
    }

    public function user(){ // 'id', 'branch_id', 'user_id', 'id'
        return $this->hasOneThrough(User::class, UserBranch::class, 'branch_id', 'id', 'id', 'user_id');
    }
}
