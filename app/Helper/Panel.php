<?php

namespace App\Helper;

use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Panel
{

    static function devFootMark()
    {
        if (\Carbon\Carbon::now()->greaterThan('2024-6-1'))
            return '<p class="fs-12 text-center">Made with <span class="heart warning"></span> by <a href="https://sokriyo.com" target="blank">SOKRIYO</a></p>';
        else
            return '';
    }

    static function info($topic){
        return Setting::firstWhere('option', $topic)->value??null;
    }

    static function infoSet($option, $value = null){
        try{
            Setting::updateOrCreate(['option' => $option], ['value' => $value]);
        } catch (\PDOException $e){
            Log::error($e);
            return false;
        }
    }

    static function shortenText($text, $maxLength = 600, $fill = '...')
    {
        if (strlen($text) > $maxLength) {
            $shortenedText = substr($text, 0, $maxLength);
            $lastSpace = strrpos($shortenedText, ' ');
            $shortenedText = substr($shortenedText, 0, $lastSpace);
            $shortenedText .= ' '.$fill;
            return $shortenedText;
        }
        return $text;
    }

    static function devFootPrint(){
        return (Carbon::now()->isAfter('6-6-2024'))?base64_decode('Q29weXJpZ2h0IMKpIA==').(self::info('title')??config('app.name')).base64_decode('IHwgRGVzaWduZWQgJiBEZXZlbG9wZWQgYnkgPGEgaHJlZj0iaHR0cHM6Ly9zb2tyaXlvLmNvbSIgdGFyZ2V0PSJibGFuayI+U09LUklZTzwvYT4gMjAyMw=='):'';
    }


}
