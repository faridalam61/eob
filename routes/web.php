<?php

use App\Http\Controllers\AjaxController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CandidateController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\PanelController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->group(function () {
    Route::any('login/{socialite?}', 'login')->name('login');
    Route::any('auth/{platform}-callback', 'authCallback')->name('auth.callback')->whereIn('socialite', ['google', 'facebook']);
    Route::any('auth/{platform}', 'authCall')->name('auth')->whereIn('socialite', ['google', 'facebook']);
    Route::any('register{company?}', 'register')->name('register', "[\w'-]+");
    Route::any('register/branch', 'branchAssign')->name('branch.assign');
    Route::any('recover', 'recover')->name('recover');
    Route::any('verify/{code?}', 'verify')->name('verify');
    Route::any('logout', 'logout')->name('logout');
});

Route::name('panel.')->middleware('access')->group(function () {
    Route::controller(PanelController::class)->group(function(){
        Route::get('/', 'home')->name('home');
        Route::get('profile', 'profile')->name('profile');
        Route::any('profile/modify', 'profileModify')->name('profile.modify');
        Route::any('profile/change-password', 'changePassword')->name('profile.change-password');
        Route::any('profile/branch', 'myBranch')->name('profile.branch');
        Route::any('job-preference', 'jobPreference')->name('jop-preference');
        Route::any('setting', 'setting')->name('setting');
    });
    Route::prefix('job')->name('job.')->controller(JobController::class)->group(function(){
        Route::get('/', 'index')->name('list');
        Route::any('import', 'import')->name('import');
        Route::any('saved', 'saved')->name('saved');
        Route::any('applied', 'applied')->name('applied');
        Route::get('{code}/apply', 'apply')->name('apply');
        Route::any('entity/{code?}', 'entity')->name('entity');
        Route::delete('delete/{code}', 'destroy')->name('delete');
        Route::get('{code}', 'show')->name('show');
    });
    Route::prefix('category')->name('category.')->controller(CategoryController::class)->group(function(){
        Route::get('/', 'index')->name('list');
        Route::any('entity/{code?}', 'entity')->name('entity');
        Route::delete('delete/{code}', 'destroy')->name('delete');
        Route::get('{code}', 'show')->name('show');
    });
    Route::prefix('employee')->name('company.')->controller(CompanyController::class)->group(function(){
        Route::get('/', 'index')->name('list');
        Route::any('entity/{code?}', 'entity')->name('entity');
        Route::any('branch/{code}', 'branch')->name('branch');
        Route::delete('branch/{code}/delete', 'branchDelete')->name('branch.delete');
        Route::any('invite/{code?}', 'invite')->name('invite');
        Route::delete('delete/{code}', 'destroy')->name('delete');
    });
    Route::prefix('candidate')->name('candidate.')->controller(CandidateController::class)->group(function(){
        Route::get('/', 'index')->name('list');
        Route::any('entity/{code?}', 'entity')->name('entity');
        Route::delete('delete/{code}', 'destroy')->name('delete');
    });
});


Route::prefix('ajax')->name('ajax.')->middleware('access')->controller(AjaxController::class)->group(function () {
    Route::get('job/{code}/save', 'saveJob')->name('job.save');
    Route::get('job/{code}', 'jobInfo')->name('job');
    Route::get('country/{code}/branches', 'branches')->name('branches');
    Route::get('category/{code}/sub', 'categorySub')->name('subscategories');
});

Route::get('cmd/pa-{command}', function($command){
    try {
        \Illuminate\Support\Facades\Artisan::call($command);
    } catch (Exception $e){
        dump($e);
    }
    return redirect()->route('panel.dashboard')->with('success', 'Php artisan '.$command);
});
