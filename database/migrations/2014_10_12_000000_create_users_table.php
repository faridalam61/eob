<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('code', 60)->unique();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->enum('type', ['admin', 'employer', 'candidate']);
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('zip_code')->nullable();
            $table->unsignedInteger('company_id')->nullable();
            $table->string('job_types')->nullable();
            $table->enum('verified', ['email', 'facebook', 'google'])->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('facebook_uuid')->nullable();
            $table->string('google_uuid')->nullable();
            $table->boolean('active')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
