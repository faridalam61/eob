<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('code', 60)->unique();

            $table->string('title');
            $table->string('subtitle');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('subcategory_id')->nullable();
            $table->longText('description');
            $table->string('type')->nullable();
            $table->string('salary_estimate')->nullable();
            $table->string('shift')->nullable();
            $table->string('benefit')->nullable();
            $table->timestamp('post_date')->nullable();
            $table->string('info_url')->nullable();
            $table->timestamp('expire_at')->nullable();
            $table->boolean('active')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jobs');
    }
};
