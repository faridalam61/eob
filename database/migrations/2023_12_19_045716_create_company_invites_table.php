<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company_invites', function (Blueprint $table) {
            $table->id();
            $table->string('code', 100)->unique();
            $table->unsignedInteger('company_id');
            $table->string('send_to'); //email address
            $table->timestamp('expire_at')->nullable();
            $table->timestamp('clicked_at')->nullable();
            $table->timestamp('activate_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company_invites');
    }
};
