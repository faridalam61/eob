<html>
<head>
<style>
    .body{
        width: 100%;
        height: 100%;
        background: whitesmoke;
        font-family: sans-serif;
    }
    .content{
        max-width: 600px;
        border: 2px solid white;
        background: lightyellow;
        box-shadow: 0 0 10px -2px darkblue;
        border-radius: 20px;
        margin: 100px auto;
    }
    .content .header {
        border-radius: 20px 20px 0 0;
        border-bottom: 2px solid gray;
        text-align: center;
        padding: 20px;
    }
    .content .body {
        background-color: inherit;
        padding: 40px 15px;
        width: initial;
        height: fit-content;
    }
    .content .body .code{
        text-align: center;
        font-size: 40px;
        font-weight: bolder;
        width: 100%;
    }
    .content .footer {
        border-radius: 0 0 20px 20px;
        border-top: 2px solid gray;
        padding: 20px;
        text-align: center;
        font-size: small;
    }
    a {
        text-decoration: none;
    }
    .center {
        text-align: center;
    }
    p.line {
        margin: 0;
        padding: 0;
    }
    li {
        margin-bottom: 15px;
    }
</style>
</head>
<body>
<div class="content">
    <div class="header">
        <a href="{{ route('panel.home') }}">{{ html()->img(asset(Panel::info('logo')), Panel::info('title'))->style('height: 50px') }}</a>
    </div>
    <div class="body">
        <p class="center">Welcome to <b>{{ Panel::info('title')??config('app.name') }}</b> registration process. </p>
        <p class="center">You are one step away to complete the registration process. We need to verify your email address and so you got this email from us. This verification link will be accept until the date {{ $date??\Carbon\Carbon::now()->addWeek()->format('d M Y') }}.</p>
        @if(isset($company) && !null == $company && $company->jobs->count())
            <p class="center">We found <b>{{ $company->name }}</b> have <i>{{ $company->jobs->count() }}</i> job listed in out portal. You can handle jobs after registration.</p>
        @endif
        <br>
        <br>
        <p class="center">
        <a href="{{ $url }}" style="font-weight: bold; text-transform: uppercase; padding: 5px 12px; background: #0a5abf; border: 1px solid blue; border-radius: 10px; color: white">Verify Now</a>
        </p>
        <br>
        <p class="center">If you are on the process of registration with acknowledge in <b>{{ Panel::info('title')??config('app.name') }}</b>, please complete theis step, otherwise just skip this email.</p>

    </div>
    <div class="footer">
        {!! Panel::devFootPrint() !!}
    </div>
</div>
</body>
</html>
