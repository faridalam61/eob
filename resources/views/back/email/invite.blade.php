<html>
<head>
<style>
    .body{
        width: 100%;
        height: 100%;
        background: whitesmoke;
        font-family: sans-serif;
    }
    .content{
        max-width: 600px;
        border: 2px solid white;
        background: lightyellow;
        box-shadow: 0 0 10px -2px darkblue;
        border-radius: 20px;
        margin: 100px auto;
    }
    .content .header {
        border-radius: 20px 20px 0 0;
        border-bottom: 2px solid gray;
        text-align: center;
        padding: 20px;
    }
    .content .body {
        background-color: inherit;
        padding: 40px 15px;
        width: initial;
        height: fit-content;
    }
    .content .body .code{
        text-align: center;
        font-size: 40px;
        font-weight: bolder;
        width: 100%;
    }
    .content .footer {
        border-radius: 0 0 20px 20px;
        border-top: 2px solid gray;
        padding: 20px;
        text-align: center;
        font-size: small;
    }
    a {
        text-decoration: none;
    }
    .center {
        text-align: center;
    }
    p.line {
        margin: 0;
        padding: 0;
    }
    li {
        margin-bottom: 15px;
    }
</style>
</head>
<body>
<div class="content">
    <div class="header">
        <a href="{{ route('panel.home') }}">{{ html()->img(asset(Panel::info('logo')), Panel::info('title'))->style('height: 50px') }}</a>
    </div>
    <div class="body">
        <p class="center">You are cordially invited to join our job portal as an employer. Your invitation code is given below and the instructions on how to register process are explained below. You can process any one of two way to register.</p>
        @if(isset($company) && !null == $company && $company->jobs->count())
            <p class="center">We found <b>{{ $company->name }}</b> have <i>{{ $company->jobs->count() }}</i> job listed in out portal. You can handle jobs after registration.</p>
        @endif
        <p class="code">{{ $code }}</p>
        <ol>
            <li>
                <p class="line">Copy the above invitation code.</p>
                <p class="line">Go to the <a href="{{ route('register', '-company') }}">REGISTRATION</a> page and past in invitation code input field.</p>
                <p class="line">Fill up other input field with your company information.</p>
                <p class="line">Press the registration button to complete registration.</p>
            </li>
            <li>
                <p class="line">Just go to this <a href="{{ route('register',  ['-company','code' => $code]) }}">REGISTRATION</a> page and fill up form and click on the registration button to complete your employer registration.</p>
            </li>
        </ol>
    </div>
    <div class="footer">
        {!! Panel::devFootPrint() !!}
    </div>
</div>
</body>
</html>
