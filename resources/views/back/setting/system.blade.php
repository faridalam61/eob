@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">

    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6 mb-3">
            <div class="card">

                    {{ html()->form('POST', route('panel.setting'))->acceptsFiles()->open() }}

                <div class="card-header text-center">
                    <h4 class="mb-0 me-auto w-100">{{ $title??'System Setting' }}</h4>
                </div>

                <div class="card-body">
                    {{ html()->label('System Name or Title', 'title')->class('form-label') }}
                    {{ html()->text('title', Panel::info('title'))->class('form-control mb-3')->placeholder('System Name')->required()->autofocus() }}

                    {{ html()->label('Tagline', 'subtitle')->class('form-label') }}
                    {{ html()->text('subtitle', Panel::info('subtitle'))->class('form-control mb-3')->placeholder('Enter tagline')->required() }}

                    <div class="row mb-3">
                        <div class="col">
                            {{ html()->label('Logo <small class="float-end">( 65 X 65 px png/svg/gif only )</small>', 'logo')->class('form-label w-100') }}
                            {{ html()->file('logo')->class('form-control mb-3')->accept('image/png, image/svg, image/gif') }}
                        </div>
                        <div class="col">
                            {{ html()->label('Favicon <small class="float-end">( 16 X 16 px png/svg/gif only )</small>', 'favicon')->class('form-label w-100') }}
                            {{ html()->file('favicon')->class('form-control mb-3')->accept('image/png, image/svg, image/gif') }}
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            {{ html()->reset('Reset')->class('btn btn-dark btn-sm') }}
                        </div>
                        <div class="col-sm-6">
                            {{ html()->submit('Update')->class('btn btn-primary btn-sm') }}
                        </div>
                    </div>
                </div>
                {{ html()->form() }}
            </div>
        </div>
    </div>

@endsection

@push('script') @endpush
