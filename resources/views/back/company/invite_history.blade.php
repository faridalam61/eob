@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">{{ $title??'Company list' }}</h3>
        <div></div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="table-responsive">
                <table class="table display mb-4 dataTablesCard company-table table-responsive-xl card-table"
                       id="example5">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Date & Time</th>
                        <th>Send To</th>
                        <th class="text-center">Invitation Code</th>
                        <th>Clicked Date</th>
                        <th>Activation Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x = $invites->firstItem())
                    @forelse($invites as $invite)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ \Carbon\Carbon::parse($invite->created_at)->format('d M Y | H:i') }}</td>
                            <td>{{ html()->mailto($invite->send_to, $invite->send_to) }}</td>
                            <td class="text-center"><span class="fw-bolder fs-22"> {{ $invite->code }}</span></td>
                            <td>
                                @if($invite->clicked_at)
                                    {{ \Carbon\Carbon::parse($invite->clicked_at)->format('d M Y | H:i') }}
                                @else
                                    <small>No clicked</small>
                                @endif
                            </td>
                            <td>
                                @if($invite->activate_at)
                                    {{ \Carbon\Carbon::parse($invite->activate_at)->format('d M Y | H:i') }}
                                @else
                                    <small>No activate</small>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="100%" class="text-center">
                                <p>No Employer exist here</p>
                                To add to this list go
                                to {{ html()->a(route('panel.company.entity'), 'Add new Company')->class('badge badge-primary badge-sm') }}
                                .
                                .
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr class="text-center">
                        <td colspan="100%">
                            {{ $invites->withQueryString()->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
