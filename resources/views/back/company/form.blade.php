@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">

    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6 mb-3">
            <div class="card">
                @if($company??false)
                    {{ html()->form('POST', route('panel.company.entity', $company->code))->open() }}
                @else
                    {{ html()->form('POST', route('panel.company.entity'))->open() }}
                @endif

                <div class="card-header text-center">
                    <h4 class="mb-0 me-auto w-100">{{ $title??'Employer form' }}</h4>
                </div>

                <div class="card-body">
                    {{ html()->label('Company Name', 'name')->class('form-label') }}
                    {{ html()->text('name', $company->name??null)->class('form-control mb-3')->placeholder('Company name')->required()->autofocus() }}

                    {{ html()->label('Website', 'info_url')->class('form-label') }}
                    {{ html()->text('info_url', $company->info_url??null)->class('form-control mb-3')->placeholder('Website')->attribute('type', 'url')->required() }}

                    {{ html()->label('Email address', 'email')->class('form-label') }}
                    {{ html()->text('email', $company->email??null)->class('form-control mb-3')->placeholder('Email address')->required() }}

                    {{ html()->label('Phone number', 'phone')->class('form-label') }}
                    {{ html()->text('phone', $company->phone??null)->class('form-control mb-3')->attribute('type', 'tel')->placeholder('Phone number')->required() }}

                    {{ html()->label('HR Contact number', 'hr_contact')->class('form-label') }}
                    {{ html()->text('hr_contact', $company->hr_contact??null)->class('form-control mb-3')->attribute('type', 'tel')->placeholder('HR phone number')->required() }}

                    {{ html()->label('Total reviews', 'review')->class('form-label') }}
                    {{ html()->number('review', $company->review??null)->class('form-control')->placeholder('Total reviews')->required() }}

                </div>
                <div class="card-footer">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            {{ html()->reset('Reset')->class('btn btn-dark btn-sm') }}
                        </div>
                        <div class="col-sm-6">
                            {{ html()->submit('Submit')->class('btn btn-primary btn-sm') }}
                        </div>
                    </div>
                </div>
                {{ html()->form() }}
            </div>
        </div>
    </div>

@endsection

@push('script') @endpush
