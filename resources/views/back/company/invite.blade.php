@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">{{ $title??'Company list' }}</h3>
        <div></div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="table-responsive">
                <table class="table display mb-4 dataTablesCard company-table table-responsive-xl card-table"
                       id="example5">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Website</th>
                        <th>Review</th>
                        <th class="text-center">Jobs</th>
                        <th class="text-center">Free Location</th>
                        <th>Status</th>
                        <th>Invite</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x = $companies->firstItem())
                    @forelse($companies as $company)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ $company->name }}</td>
                            <td><small>{{ $company->email }}<br>{{ $company->hr_contact }}</small></td>
                            @php($domain = parse_url($company->info_url)['host']??'Not define')
                            <td>{{ $company->info_url? html()->a($company->info_url,$domain)->target('blank') : $company->info_url }}</td>
                            <td>{{ $company->review }}</td>
                            <td class="text-center">{{ $company->jobs->count() }}</td>
                            <td class="text-center">
                                {{ html()->a(route('panel.company.branch', $company->code), $company->branches()->free()->count())->class('badge badge-secondary badge-sm') }}
                            </td>
                            <td>
                                @if($company->user)
                                    <span class="badge badge-secondary badge-lg light">Joined</span>
                                @elseif($company->invites->count())
                                    <span class="badge badge-danger badge-md light">{{ $company->invites->count() }} invitation sent </span>
                                @else
                                    <span class="badge badge-dark badge-lg light"> Not Invited</span>
                                @endif
                            </td>
                            <td>
                                <div class="action-buttons d-flex justify-content-end">
                                    @if($company->invites->count())
                                    <a href="{{ route('panel.company.invite', $company->code) }}"
                                       class="btn btn-success btn-sm light mr-2" title="Invitation History">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    @endif
                                    <a data-bs-toggle="modal" data-bs-target="#invite{{ $company->code }}"
                                       class="btn btn-secondary btn-sm light mr-2" title="Invite Now">
                                        <i class="fa fa-paper-plane"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @php($dependency = ($company->jobs->count() || $company->branches->count())?true:false )
                        <div class="modal fade" id="invite{{ $company->code }}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {{ html()->form('post', route('panel.company.invite', $company->code))->open() }}
                                    <div class="modal-header">
                                        <h5 class="modal-title">Send new invitation</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal">
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-auto">
                                                {{ html()->label('Invite Code', 'code')->class('form-label') }}
                                                {{ html()->number('code', $company->id.rand(1000, 9999))->class('form-control text-center p-0 bg-light fw-bold text-black')->placeholder('Code')->required()->style('width:5rem') }}
                                            </div>
                                            <div class="col">
                                                {{ html()->label('Email address', 'email')->class('form-label') }}
                                                {{ html()->email('email', $company->email??null)->class('form-control')->placeholder('Recipient email address') }}
                                            </div>
                                        </div>
                                        <br>
                                        <p class="mb-0">You are about to invite <b class="text-primary">{{ $company->name }}</b> to register as an employer. This action will trigger an email to the recipient containing registration instructions</p>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row" style="display: contents">
                                            <div class="col">
                                                {{ html()->button('<i class="fa fa-close"></i>')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                            </div>
                                            <div class="col text-end">
                                                    {{ html()->submit('<i class="fa fa-paper-plane"></i>')->class('btn btn-primary btn-sm') }}
                                            </div>
                                        </div>
                                    </div>
                                    {{ html()->form()->close() }}
                                </div>
                            </div>
                        </div>
                    @empty
                        <tr>
                            <td colspan="100%" class="text-center">
                                <p>No Employer exist here</p>
                                To add to this list go
                                to {{ html()->a(route('panel.company.entity'), 'Add new Company')->class('badge badge-primary badge-sm') }}
                                .
                                .
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr class="text-center">
                        <td colspan="100%">
                            {{ $companies->withQueryString()->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
