@php($authUser = auth()->user())
@php($guard = $authUser->type??null)

@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">{{ $title??'Company list' }}</h3>
        <div>
            @if($guard == 'admin')
            <a href="{{ route('panel.company.entity') }}" class="btn btn-primary btn-xs me-3"><i
                    class="fas fa-plus me-2"></i>Add New Employer</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="table-responsive">
                <table class="table display mb-4 dataTablesCard company-table table-responsive-xl card-table"
                       id="example5">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Website</th>
                        <th>Review</th>
                        <th class="text-center">Jobs</th>
                        <th class="text-center">Location</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x = $companies->firstItem())
                    @forelse($companies as $company)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ $company->name }}</td>
                            @php($domain = parse_url($company->info_url)['host']??'Not define')
                            <td>{{ $company->info_url? html()->a($company->info_url,$domain)->target('blank') : $company->info_url }}</td>
                            <td>{{ $company->review }}</td>
                            <td class="text-center">{{ html()->a(route('panel.job.list', ['company_id' => $company->id]), $company->jobs->count())->class('badge badge-secondary badge-sm') }}</td>
                            <td class="text-center">
                                @if($company->branches()->assigned()->count() == $company->branches->count())
                                    {{ html()->a(route('panel.company.branch', $company->code), $company->branches->count())->class('badge badge-primary badge-sm fw-bold')->attributes(['data-toggle' => "tooltip", 'data-placement' => "top", 'title' => "All Location Assigned "]) }}
                                @else
                                    <span class="badge badge-default badge-xs" data-toggle="tooltip" data-placement="top" title="Assigned location">{{ $company->branches()->assigned()->count() }}</span>/
                                    {{ html()->a(route('panel.company.branch', $company->code), $company->branches->count())->class('badge badge-secondary badge-sm') }}
                                @endif
                            </td>
                            <td>
                                @if($company->user)
                                    <span class="badge badge-secondary badge-lg light">Joined</span>
                                @elseif($company->invites->count())
                                    <span class="badge badge-danger badge-lg light">Invited ({{ $company->invites->count() }}) </span>
                                @else
                                    <span class="badge badge-dark badge-lg light"> Not Invited</span>
                                @endif
                            </td>
                            <td>
                                <div class="action-buttons d-flex justify-content-end">
                                    {{--                                    <a href="{{ route('panel.company.show', $company->code) }}" class="btn btn-success btn-sm light mr-2" title="Details">--}}
                                    {{--                                        <i class="fa fa-eye"></i>--}}
                                    {{--                                    </a>--}}
                                    <a href="{{ route('panel.company.entity', $company->code) }}"
                                       class="btn btn-secondary btn-sm light mr-2" title="Modify"><i
                                            class="fa fa-pencil"></i></a>
                                    @if($guard == 'admin')
                                    <a class="btn btn-danger btn-sm light" title="Delete" data-bs-toggle="modal"
                                       data-bs-target="#delete{{ $company->code }}"><i class="fa fa-trash"></i> </a>
                                        @endif
                                </div>
                            </td>
                        </tr>
                        @if($guard == 'admin')
                        @php($dependency = ($company->jobs->count() || $company->branches->count())?true:false )
                        <div class="modal fade" id="delete{{ $company->code }}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {{ html()->form('delete', route('panel.company.delete', $company->code))->open() }}
                                    <div class="modal-header">
                                        <h5 class="modal-title">Delete confirmation</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal">
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        @if($dependency)
                                            <i
                                                class="text-info">{{ $company->title }}</i> employer can't be deleted. Remove all the dependencies (eg: location, Job, invitation) first  then try again.
                                        @else
                                            Do you want to delete  <i
                                                class="text-info">{{ $company->title }}</i>  permanently ? 
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row" style="display: contents">
                                            <div class="col">
                                                {{ html()->button('Close')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                            </div>
                                            <div class="col text-end">
                                                @if($dependency)
                                                    {{ html()->button('Delete !')->class('btn btn-dark btn-sm')->type('button')->disabled() }}
                                                @else
                                                    {{ html()->submit('Delete')->class('btn btn-primary btn-sm') }}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    {{ html()->form()->close() }}
                                </div>
                            </div>
                        </div>
                        @endif
                    @empty
                        <tr>
                            <td colspan="100%" class="text-center">
                                <p>Entries not found</p>
                                @if($guard == 'admin')
                                
                                    to {{ html()->a(route('panel.company.entity'), 'Add new Company')->class('badge badge-primary badge-sm') }}
                                    ..
                                @endif
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr class="text-center">
                        <td colspan="100%">
                            {{ $companies->withQueryString()->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
