@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">
        <h3 class="mb-0 me-auto">{{ $title }}</h3>
    </div>
    <div class="card">
        @if($candidate??false)
            {{ html()->form('POST', route('panel.candidate.entity', $candidate->code))->open() }}
        @else
            {{ html()->form('POST', route('panel.candidate.entity'))->open() }}
        @endif

        <div class="card-header d-none">
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <h4 class="text-center border-bottom">User information</h4>
                    {{ html()->label('First Name', 'first_name')->class('form-label') }}
                    {{ html()->text('first_name', $candidate->first_name??null)->class('form-control mb-3')->placeholder('Enter first name')->required()->autofocus() }}

                    {{ html()->label('Last Name', 'last_name')->class('form-label') }}
                    {{ html()->text('last_name', $candidate->last_name??null)->class('form-control mb-3')->placeholder('Enter last name') }}

                    {{ html()->label('Email Address', 'email')->class('form-label') }}
                    {{ html()->email('email', $candidate->email??null)->class('form-control mb-3')->placeholder('Enter email address')->required() }}

                    {{ html()->label('Phone number', 'phone')->class('form-label') }}
                    {{ html()->text('phone', $candidate->phone??null)->class('form-control mb-3')->attribute('type', 'tel')->placeholder('Enter phone number')->required() }}

                    @if($candidate??false)
                        {{ html()->label('Password <small class="float-end"> Keep blank to un-change previous</small>', 'password')->class('form-label w-100') }}
                        {{ html()->password('password')->class('form-control mb-3')->placeholder('Enter password') }}

                        {{ html()->label('Confirm Password', 'password_confirmation')->class('form-label') }}
                        {{ html()->password('password_confirmation')->class('form-control mb-3')->placeholder('Confirm password') }}
                    @else
                        {{ html()->label('Password <small class="float-end"> Minimum 6 character</small>', 'password')->class('form-label w-100') }}
                        {{ html()->password('password')->class('form-control mb-3')->placeholder('Enter password')->required() }}

                        {{ html()->label('Confirm Password', 'password_confirmation')->class('form-label') }}
                        {{ html()->password('password_confirmation')->class('form-control mb-3')->placeholder('Confirm pasword')->required() }}
                    @endif
                </div>
                <div class="col-sm-6 mb-3">
                    <h4 class="text-center border-bottom">Job Categories</h4>
                    <p class="text-center">Choose your preferred categories to view job listings and receive recommendations.</p>
                    @php($catIds = isset($candidate)?$candidate->categories->pluck('id')->toArray():[])
                    @forelse($categories??[] as $category)
                        <div class="form-check custom-checkbox mb-3 checkbox-danger">
                            {{ html()->checkbox('categories[]', in_array($category->id, $catIds)?true:false, $category->code)->class('form-check-input')->placeholder('Enter sub-title')->attribute('id', $category->code) }}
                            {{ html()->label($category->name, $category->code)->class('form-check-label') }}
                        </div>
                    @empty
                        <p class="text-center">Entries not found</p>
                    @endforelse
                </div>

            </div>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-sm-6">
                    {{ html()->reset('Reset')->class('btn btn-dark btn-sm') }}
                </div>
                <div class="col-sm-6">
                    {{ html()->submit('Submit')->class('btn btn-primary btn-sm') }}
                </div>
            </div>
        </div>
        {{ html()->form() }}
    </div>

@endsection

@push('script') @endpush
