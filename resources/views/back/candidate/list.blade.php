@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">{{ $title??'Employee List' }}</h3>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="table-responsive">
                <table class="table display mb-4 dataTablesCard job-table table-responsive-xl card-table" id="example5">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Register at</th>
                        <th>Applied</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x = $candidates->firstItem())
                    @forelse($candidates as $candidate)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ html()->a('mailto:'.$candidate->email, $candidate->email) }}</td>
                            <td>{{ $candidate->first_name.' '.$candidate->last_name }}</td>
                            <td>{{ html()->tel($candidate->phone,$candidate->phone)  }}</td>
                            <td>{{ \Carbon\Carbon::parse($candidate->created_at)->format('Y M d') }}</td>
                            <td>{{ $candidate->applied_jobs->count()??0 }}</td>
                            <td>
                                <div class="action-buttons d-flex justify-content-end">
{{--                                    <a href="{{ route('panel.candidate.show', $candidate->code) }}" class="btn btn-success btn-sm light mr-2" title="Details"><i class="fa fa-eye"></i></a>--}}
                                    <a href="{{ route('panel.candidate.entity', $candidate->code) }}" class="btn btn-secondary btn-sm light mr-2" title="Modify"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger btn-sm light" title="Delete" data-bs-toggle="modal" data-bs-target="#delete{{ $candidate->code }}"><i class="fa fa-trash"></i> </a>
                                </div>
                            </td>
                        </tr>
                        <div class="modal fade" id="delete{{ $candidate->code }}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {{ html()->form('delete', route('panel.candidate.delete', $candidate->code))->open() }}
                                    <div class="modal-header">
                                        <h5 class="modal-title">Delete confirmation</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal">
                                        </button>
                                    </div>
                                    <div class="modal-body">Do you want to delete  <i class="text-info">{{ $candidate->name }}</i> permanently ? </div>
                                    <div class="modal-footer">
                                        <div class="row" style="display: contents">
                                            <div class="col">
                                                {{ html()->button('Close')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                            </div>
                                            <div class="col text-end">
                                                {{ html()->submit('Delete')->class('btn btn-primary btn-sm') }}
                                            </div>
                                        </div>
                                    </div>
                                    {{ html()->form()->close() }}
                                </div>
                            </div>
                        </div>
                    @empty
                        <tr>
                            <td colspan="100%" class="text-center">
                                <p>Entries not found</p>
                               {{ html()->a(route('panel.candidate.entity'), 'Add new user')->class('badge badge-primary badge-sm') }}
                                
                                .
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr class="text-center">
                        <td colspan="100%">
                            {{ $candidates->withQueryString()->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
