@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">

    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6 mb-3">
            <div class="card">
                @if($category??false)
                    {{ html()->form('POST', route('panel.category.entity', $category->code))->open() }}
                @else
                    {{ html()->form('POST', route('panel.category.entity'))->open() }}
                @endif

                <div class="card-header text-center">
                    <h4 class="mb-0 me-auto w-100">{{ $title??'Employer form' }}</h4>
                </div>

                <div class="card-body">
                    @if(request()->parentId??false)
                        {{ html()->hidden('parentId', request()->parentId) }}
                    @endif

                    {{ html()->label('Category Name', 'name')->class('form-label') }}
                    {{ html()->text('name', $category->name??null)->class('form-control mb-3')->placeholder('Input category name')->required()->autofocus() }}

                </div>
                <div class="card-footer">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            {{ html()->reset('Reset')->class('btn btn-dark btn-sm') }}
                        </div>
                        <div class="col-sm-6">
                            {{ html()->submit('Submit')->class('btn btn-primary btn-sm') }}
                        </div>
                    </div>
                </div>
                {{ html()->form() }}
            </div>
        </div>
    </div>

@endsection

@push('script') @endpush
