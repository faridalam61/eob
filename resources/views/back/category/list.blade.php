@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">{{ $title??'Location list' }}</h3>
        <div>
            @if($parentId??false)
            <a href="{{ route('panel.category.entity', ['parentId' => $parentId]) }}" class="btn btn-primary btn-xs me-3">
                <i class="fas fa-plus me-2"></i>Add New SubCategory</a>
            @else
            <a href="{{ route('panel.category.entity') }}" class="btn btn-primary btn-xs me-3">
                <i class="fas fa-plus me-2"></i>Add New Category</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="table-responsive">
                <table class="table display mb-4 dataTablesCard location-table table-responsive-xl card-table"
                       id="example5">
                    <thead>
                    <tr>
                        <th style="white-space: nowrap; width: 1%">No</th>
                        <th>Name</th>
                        <th class="text-center">{{ ($parentId??false)?'Parent':'Sub' }} Categories</th>
                        <th class="text-center">Jobs</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x = $categories->firstItem())
                    @forelse($categories as $category)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ $category->name }}</td>
                            <td class="text-center">
                                @if($category->parent_id)
                                    {{ $category->parent->name }}
                                @else
                                    {{ html()->a(route('panel.category.list', ['parent_id' => $category->code]), $category->children->count()??0)->class('badge badge-secondary badge-xs') }}
                                @endif
                            </td>
                            <td class="text-center">
                                {{ html()->a(route('panel.job.list', ['category_id' => $category->id]), $category->jobs->count())->target('blank')->class('badge badge-secondary badge-xs') }}
                            </td>
                            <td style="white-space: nowrap; width: 5%">
                                <div class="action-buttons d-flex justify-content-end">
                                    <a href="{{ route('panel.category.entity', $category->code) }}"
                                       class="btn btn-secondary btn-sm light mr-2" title="Modify">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-danger btn-sm light" title="Delete" data-bs-toggle="modal"
                                       data-bs-target="#delete{{ $category->code }}"><i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>

                    @empty
                        <tr>
                            <td colspan="100%" class="text-center">
                                <p>Entires not found</p> 
                                @if($parentId??false)
                                    {{ html()->a(route('panel.category.entity', ['parentId' => $parentId]), '<i class="fas fa-plus me-2"></i> Add new SubCategory')->class('badge badge-primary badge-sm') }}
                                @else
                                    {{ html()->a(route('panel.category.entity'), '<i class="fas fa-plus me-2"></i> Add new Category')->class('badge badge-primary badge-sm') }}
                                @endif
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr class="text-center">
                        <td colspan="100%">
                            {{ $categories->withQueryString()->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            @forelse($categories as $category)
                @php($refference = ($category->jobs->count() || $category->children->count())?1:0)
                <div class="modal fade" id="delete{{ $category->code }}">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete confirmation</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                            </div>
                            <div class="modal-body">
                                <p class="m-0">
                                    @if($refference)
                                        You can not delete <i class="text-info">{{ $category->name }}</i>
                                        
                                    @else
                                        Do you want to delete  <i class="text-info">{{ $category->location }}</i>
                                         permanently ?
                                    @endif
                                </p>
                            </div>
                            <div class="modal-footer">
                                <div class="row" style="display: contents">
                                    <div class="col">
                                        {{ html()->button('Close')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                    </div>
                                    <div class="col text-end">
                                        @if($refference)
                                            {{ html()->button('Delete !')->class('btn btn-dark  btn-sm')->type('button')->disabled() }}
                                        @else
                                            {{ html()->form('delete', route('panel.category.delete', $category->code))->open() }}
                                            {{ html()->submit('Delete')->class('btn btn-primary btn-sm') }}
                                            {{ html()->form()->close() }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
@endsection

@push('script') @endpush
