@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">{{ $title??'Location list' }}</h3>
        <div>
            <span role="button" data-bs-toggle="modal" data-bs-target="#addNew" class="btn btn-primary btn-xs me-3">
                <i class="fas fa-plus me-2"></i>Add New Location</span>
            <div class="modal fade" id="addNew">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {{ html()->form('post', route('panel.company.branch', $company->code))->open() }}
                        <div class="modal-header">
                            <h5 class="modal-title">Add new location</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal">
                            </button>
                        </div>
                        <div class="modal-body">
                            {{ html()->label('New Location / Branch name', 'location')->class('form-label') }}
                            {{ html()->text('location')->class('form-control mb-3')->placeholder('Input new branch name')->required() }}
                        </div>
                        <div class="modal-footer">
                            <div class="row" style="display: contents">
                                <div class="col">
                                    {{ html()->button('Close')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                </div>
                                <div class="col text-end">
                                    {{ html()->submit('Add')->class('btn btn-primary btn-sm') }}
                                </div>
                            </div>
                        </div>
                        {{ html()->form()->close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="table-responsive">
                <table class="table display mb-4 dataTablesCard location-table table-responsive-xl card-table"
                       id="example5">
                    <thead>
                    <tr>
                        <th style="white-space: nowrap; width: 1%">No</th>
                        <th>Location</th>
                        <th class="text-center">Jobs</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x = $branches->firstItem())
                    @forelse($branches as $branch)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ $branch->location }}</td>
                            <td class="text-center">
                                {{ html()->a(route('panel.job.list', ['branch_id' => $branch->id]), $branch->jobs->count())->target('blank')->class('badge badge-secondary badge-xs') }}</td>
                            <td>
                                @if($branch->user)
                                    <span class="badge badge-secondary badge-lg light"> User Linked </span>
                                @else
                                    <span class="badge badge-dark badge-lg light"> Not Linked</span>
                                @endif
                            </td>
                            <td style="white-space: nowrap; width: 5%">
                                <div class="action-buttons d-flex justify-content-end">
                                    <a href="{{ route('panel.company.entity', $branch->code) }}"
                                       class="btn btn-secondary btn-sm light mr-2" title="Modify" data-bs-toggle="modal"
                                       data-bs-target="#edit{{ $branch->code }}"><i
                                            class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger btn-sm light" title="Delete" data-bs-toggle="modal"
                                       data-bs-target="#delete{{ $branch->code }}"><i class="fa fa-trash"></i> </a>
                                </div>
                            </td>
                        </tr>

                    @empty
                        <tr>
                            <td colspan="100%" class="text-center">
                                <p>No Location exist here</p>
                                To add to this list go
                                to {{ html()->a(route('panel.company.entity'), 'Add new Location')->class('badge badge-primary badge-sm') }}
                                .
                                .
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr class="text-center">
                        <td colspan="100%">
                            {{ $branches->withQueryString()->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            @forelse($branches as $branch)
                <div class="modal fade" id="edit{{ $branch->code }}">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {{ html()->form('post', route('panel.company.branch', $company->code))->open() }}
                            {{ html()->hidden('branchCode', $branch->code) }}
                            <div class="modal-header">
                                <h5 class="modal-title">Modify location</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal">
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ html()->label('New Location / Branch name', 'location')->class('form-label') }}
                                {{ html()->text('location', $branch->location)->class('form-control mb-3')->placeholder('Input new branch name')->required() }}
                            </div>
                            <div class="modal-footer">
                                <div class="row" style="display: contents">
                                    <div class="col">
                                        {{ html()->button('Close')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                    </div>
                                    <div class="col text-end">
                                        {{ html()->submit('Update')->class('btn btn-primary btn-sm') }}
                                    </div>
                                </div>
                            </div>
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="delete{{ $branch->code }}">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete confirmation</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                            </div>
                            <div class="modal-body">
                                <p class="m-0">
                                @if($branch->jobs->count())
                                    You can not delete this <i class="text-info">{{ $branch->location }}</i>
                                    location due to job reference. ? To delete need to remove alla reference job
                                    first.
                                @else
                                    Do you want to delete this <i class="text-info">{{ $branch->location }}</i>
                                    location permanently ? To agree press delete button otherwise cancel now.
                                @endif
                                </p>
                            </div>
                            <div class="modal-footer">
                                <div class="row" style="display: contents">
                                    <div class="col">
                                        {{ html()->button('Close')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                    </div>
                                    <div class="col text-end">
                                        @if($branch->jobs->count()??0)
                                            {{ html()->button('Delete !')->class('btn btn-dark  btn-sm')->type('button')->disabled() }}
                                        @else
                                            {{ html()->form('delete', route('panel.company.branch', $company->code))->open() }}
                                            {{ html()->hidden('branchCode', $branch->code) }}
                                            {{ html()->submit('Delete')->class('btn btn-primary btn-sm') }}
                                            {{ html()->form()->close() }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
@endsection

@push('script') @endpush
