<script src="{{ asset('back/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('back/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('back/js/custom.min.js') }}"></script>
<script src="{{ asset('back/js/dlabnav-init.js') }}"></script>
<script src="{{ asset('back/vendor/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('back/vendor/toastr/js/toastr.min.js') }}"></script>

<script>
    $(document).ready(function () {
        @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr["error"]("{!! $error !!}");
        @endforeach
            @endif
            @if(Session::has('success'))
            toastr["success"]("{!! Session::pull('success') !!}");
        @endif
            @if(Session::has('info'))
            toastr["info"]("{!! Session::pull('info') !!}");
        @endif
            @if(Session::has('warning'))
            toastr["warning"]("{!! Session::pull('warning') !!}");
        @endif
            @if(Session::has('error'))
            toastr["error"]("{!! Session::pull('error') !!}");
        @endif
    });
</script>
