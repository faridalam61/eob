<link href="{{ asset(Panel::info('favicon')??'back/images/favicon.png') }}" type="image/png" rel="shortcut icon">
<link href="{{ asset('back/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet') }}">
<link href="{{ asset('back/vendor/select2/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('back/vendor/toastr/css/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('back/css/style.css') }}" rel="stylesheet">
