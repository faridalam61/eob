<!-- All StyleSheet -->
<link href="{{ asset('back/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
<link href="{{ asset('back/vendor/owl-carousel/owl.carousel.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('back/vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('back/vendor/toastr/css/toastr.min.css') }}">
<!-- Globle CSS -->
<link href="{{ asset('back/css/style.css') }}" rel="stylesheet">
