<!--**********************************
	Scripts
***********************************-->
<!-- Required vendors -->
<script src="{{ asset('back/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('back/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>

<!-- Apex Chart -->
<script src="{{ asset('back/vendor/apexchart/apexchart.js') }}"></script>
<script src="{{ asset('back/vendor/chartjs/chart.bundle.min.js') }}"></script>

<!-- Chart piety plugin files -->
<script src="{{ asset('back/vendor/peity/jquery.peity.min.js') }}"></script>

<!-- Dashboard 1 -->
<script src="{{ asset('back/js/dashboard/dashboard-1.js') }}"></script>

<script src="{{ asset('back/vendor/owl-carousel/owl.carousel.js') }}"></script>

<script src="{{ asset('back/js/custom.min.js') }}"></script>
<script src="{{ asset('back/js/dlabnav-init.js') }}"></script>
<script src="{{ asset('back/vendor/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('back/vendor/toastr/js/toastr.min.js') }}"></script>

<script>
    $(document).ready(function () {
        @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr["error"]("{!! $error !!}");
            @endforeach
        @endif
        @if(Session::has('success'))
            toastr["success"]("{!! Session::pull('success') !!}");
        @endif
            @if(Session::has('info'))
            toastr["info"]("{!! Session::pull('info') !!}");
        @endif
            @if(Session::has('warning'))
            toastr["warning"]("{!! Session::pull('warning') !!}");
        @endif
            @if(Session::has('error'))
            toastr["error"]("{!! Session::pull('error') !!}");
        @endif
    });
</script>
<script>
    function JobickCarousel()
    {

        /*  testimonial one function by = owl.carousel.js */
        jQuery('.front-view-slider').owlCarousel({
            loop:false,
            margin:30,
            nav:true,
            autoplaySpeed: 3000,
            navSpeed: 3000,
            autoWidth:true,
            paginationSpeed: 3000,
            slideSpeed: 3000,
            smartSpeed: 3000,
            autoplay: false,
            animateOut: 'fadeOut',
            dots:true,
            navText: ['', ''],
            responsive:{
                0:{
                    items:1,

                    margin:10
                },

                480:{
                    items:1
                },

                767:{
                    items:3
                },
                1750:{
                    items:3
                }
            }
        })
    }

    jQuery(window).on('load',function(){
        setTimeout(function(){
            JobickCarousel();
        }, 1000);
    });
</script>
