@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row shapreter-row justify-content-center">
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
                                <span><i class="fas fa-flag"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\Job::count() }}</h3>
                                <p class="mb-0">Total Job</spapn>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
                                <span><i class="fas fa-check"></i></span>
                                <h3 class="count mb-0">{{ Auth::user()->branches->count() }}</h3>
                                <p class="mb-0">Total Employee</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
                                <span><i class="fas fa-save"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\CandidateJob::whereIn('job_id', \App\Models\Job::pluck('id'))->where('saved_at')->count() }}</h3>
                                <p class="mb-0">Saved Job</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
                                <span><i class="fas fa-paper-plane"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\CandidateJob::whereIn('job_id', \App\Models\Job::pluck('id'))->where('applied_at')->count() }}</h3>
                                <p class="mb-0">Applied Job</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('style') @endpush
