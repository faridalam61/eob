@extends('back.layout')

@push('style')
    <style>
        .job-card.selected {
            background-color: rgba(255, 182, 193, 0.1);
        }

        .job-card .job-header {
            border-bottom: 1px solid var(--primary);
        }

        .job-card .job-footer {
            border-top: 1px solid var(--primary);
        }
    </style>
@endpush

@section('content')
    <div class="job-info d-none">
        <div class="job-card border rounded border-primary mb-4 position-sticky job-view"
             id="JOBCODE">
            <div class="job-header border-bottom border-primary px-3 py-1">
                <div class="div">
                    <div class="row">
                        <div class="col">
                            <h4 class="d-block w-100 p-0 m-0">TITLE</h4>
                            <p class="w-100 p-0 m-0">SUBTITLE</p>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-default btn-xs p-1 fs-20 save-now"
                                    code="JOBCODE"><i class="fa-solid fa-close"></i></button>
                        </div>
                    </div>
                    <div class="d-block">
                            <a href="{{ route('panel.home', ['category' => 'CATCODE']) }}" class="sec-cat">
                                <i class="fa fa-code-branch"></i> CATNAME</a>
                            <a href="{{ route('panel.home', ['subcategory' => 'CATSUBCODE']) }}" class="sec-subcat">
                                <i class="fa fa-circle-arrow-right"></i> CATSUBNAME
                            </a>
                    </div>
                    <div class="d-block py-1">
                            <a href="{{ route('panel.home', ['employer' => 'EMPCODE']) }}" class="sec-emp">
                                <i class="fa fa-building"></i> EMPNAME</a>
                            <a href="{{ route('panel.home', ['location' => 'LOCCODE']) }}" class="sec-loc">
                                <i class="fa fa-map-marker"></i> LOCNAME</a>
                    </div>
                    <div class="row border-top py-2">
                        <div class="col job-card">
                            <a class="btn btn-primary btn-sm sec-apply" href="{{ route('panel.job.apply', 'JOBCODE') }}" target="blank">APPLY-NOW
                            </a>
                            <a class="btn btn-sm save-now btn-outline-primary sec-saved" code="JOBCODE" style="margin-left: 10px">
                                <i class="fa-solid fa-save"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="job-body p-3">
                <div class="sec-details">
                <h3>Job Details</h3>
                <p>DETAILS</p>
                </div>
                <div class="sec-salary">
                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Salary</h3>
                <p>SALARY</p>
                </div>
                <div class="sec-shift">
                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Shift</h3>
                <p>SHIFT</p>
                </div>
                <div class="sec-benefit">
                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Benefit</h3>
                <p>BENEFIT</p>
                </div>
            </div>

                <div class="job-footer px-3 sec-expire">
                    <div class="text-center">
                        <i><small>Application wll be accept
                                till EXPIRE</small></i>
                    </div>
                </div>

        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="w-100 text-primary">Search</h4> 
            <div class="row justify-content-center w-100" style="display: contents">
                <div class="col-xs-11 col-sm-10 col-md-9 col-lg-8">
                    {{ html()->form('GET', route('panel.home'))->class('w-100')->open() }}
                    <div class="input-group">
                        <span class="input-group-text"><i class="fa-solid fa-flag"></i></span>
                        {{ html()->text('keyword', request()->keyword)->class('form-control')->placeholder('Job title, keywords, or company')->autofocus() }}
                        <span class="input-group-text"><i class="fa-solid fa-map-marker"></i></span>
                        {{ html()->text('location', request()->location)->class('form-control')->placeholder('City, state, zip code, or &quot;remote&quot;') }}
                        {{ html()->submit('<i class="fa-solid fa-search"></i>')->class('input-group-text btn btn-primary') }}
                    </div>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
        <div class="card-body" style="min-height: 90vh">
            <div class="row">
                <div class="col">
                    @forelse($jobs??[] as $job)
                        <div class="job-card border rounded border-primary mb-4" data-code="{{ $job->code }}" role="button">
                            <div class="job-header border-bottom border-primary px-3 py-1">
                                <div class="div">
                                    <div class="row">
                                        <div class="col">
                                            <h4 class="d-block w-100 p-0 m-0">{{ $job->title }}</h4>
                                            <p class="w-100 p-0 m-0">{{ $job->subtitle }}</p>
                                        </div>
                                        <div class="col-auto">
                                            @if(true)
                                                <button
                                                    class="btn btn-xs p-1 fs-20 save-now {{ $job->is_saved?'btn-primary':'btn-outline-primary' }}"
                                                    code="{{ $job->code }}"><i class="fa-solid fa-save"></i></button>
                                            @else
                                                <div class="btn-group">
                                            <span type="button" class="text-primary dropdown-toggle fs-20"
                                                  data-bs-toggle="dropdown"
                                                  aria-expanded="false"><i
                                                    class="fa-solid fa-ellipsis-vertical"></i></span>
                                                    <div class="dropdown-menu dropdown-menu-xs" style="">
                                                        {{ html()->a(route('register'), '<i class="fa-solid fa-save"></i> Save')->class('text-primary dropdown-item dropdown-item-sm') }}
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="job-body p-3">
                                <p>{{ Panel::shortenText(strip_tags($job->description)) }}</p>
                            </div>
                            <div class="job-footer px-3">
                                <div class="row">
                                    <div class="col small">
                                        Posted {{ \Carbon\Carbon::parse($job->created_at)->diffForHumans() }}</div>
                                    @if($job->company)
                                        <div class="col text-end small">Employer - <a
                                                href="{{ route('panel.home', ['employer' => $job->company->code]) }}">{{ $job->company->name??'' }}</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="text-center mt-5">     
                        No available positions found.
                        </div>
                    @endforelse
                </div>
                @php($job = $jobs->first())
                @if(isset($jobs) && $jobs->count()??false)
                    <div class="col job-box">

                        <div class="border rounded border-primary mb-4 position-sticky job-view"
                             id="{{ $job->code }}">
                            <div class="job-header border-bottom border-primary px-3 py-1">
                                <div class="div">
                                    <div class="row">
                                        <div class="col">
                                            <h4 class="d-block w-100 p-0 m-0">{{ $job->title }}</h4>
                                            <p class="w-100 p-0 m-0">{{ $job->subtitle }}</p>
                                        </div>
                                        <div class="col-auto">
                                            <button class="btn btn-default btn-xs p-1 fs-20"
                                                    code="{{ $job->code }}"><i class="fa-solid fa-close"></i></button>
                                        </div>
                                    </div>
                                    <div class="d-block">
                                        @if($job->category)
                                            <a href="{{ route('panel.home', ['category' => $job->category->code]) }}">
                                                <i class="fa fa-code-branch"></i> {{ $job->category->name }}</a>
                                        @endif
                                        @if($job->subcategory)
                                            <a href="{{ route('panel.home', ['category' => $job->subcategory->code]) }}">
                                                <i class="fa fa-circle-arrow-right"></i> {{ $job->subcategory->name }}
                                            </a>
                                        @endif
                                    </div>
                                    <div class="d-block py-1">
                                        @if($job->company)
                                            <a href="{{ route('panel.home', ['employer' => $job->company->code]) }}">
                                                <i class="fa fa-building"></i> {{ $job->company->name }}</a>
                                        @endif
                                        @if($job->branch)
                                            <a href="{{ route('panel.home', ['location' => $job->branch->code]) }}">
                                                <i class="fa fa-map-marker"></i> {{ $job->branch->location }}</a>
                                        @endif
                                    </div>
                                    <div class="row border-top py-2">
                                        <div class="col">
                                            <a class="btn btn-primary btn-sm"
                                               href="{{ route('panel.job.apply', $job->code) }}" target="blank">Apply
                                                Now</a>
                                            <button
                                                class="btn btn-sm save-now {{ $job->is_saved?'btn-primary':'btn-outline-primary' }}"
                                                code="{{ $job->code }}" style="margin-left: 10px"><i
                                                    class="fa-solid fa-save"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="job-body p-3">
                                <h3>Job Details</h3>
                                <p>{!! $job->description !!}</p>
                                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Salary</h3>
                                <p>{{ $job->salary_estimate }}</p>
                                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Shift</h3>
                                <p>{{ $job->shift }}</p>
                                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Benefit</h3>
                                <p>{{ $job->benefit }}</p>
                            </div>
                            @if($job->expire_at)
                                <div class="job-footer px-3">
                                    <div class="text-center">
                                        <i><small>Application wll be accept
                                                till {{ \Carbon\Carbon::parse($job->expire_at)->format('d M Y') }}</small></i>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                @endif
            </div>
        </div>
        <div class="card-footer">
            {{ $jobs->withQueryString()->links() }}
        </div>
    </div>

@endsection

@push('script')
    <script>
        $(".job-card .save-now, .job-view .save-now").click(function (e) {
            var url = "{{ route('ajax.job.save', 'CODE') }}";
            var button = $(this).closest('button');
            url = url.replace('CODE', $(this).attr('code'));
            $.ajax({
                url: url.replace('CODE', $(this).attr('code')), success: function (result) {
                    var replay = $.parseJSON(result);
                    console.log(replay.saved);
                    if (!null == replay.saved) {
                        button.removeClass('btn-outline-primary').addClass('btn-primary');
                    } else {
                        button.addClass('btn-outline-primary').removeClass('btn-primary');
                    }
                }
            });
            e.preventDefault();
        });
        $(".job-card").click(function () {
            $(".job-card").not(this).removeClass('selected');
            $(this).addClass('selected');

            var url = "{{ route('ajax.job', 'CODE') }}";
            url = url.replace('CODE', $(this).attr('data-code'));

            $.ajax({
                url: url, success: function (result) {
                    var response = $.parseJSON(result);
                    var sampleContent = $(".job-info").html();
                    sampleContent = sampleContent.replaceAll('JOBCODE', response.job.code);
                    sampleContent = sampleContent.replaceAll('TITLE', response.job.title);
                    sampleContent = sampleContent.replaceAll('SUBTITLE', response.job.subtitle);
                    sampleContent = sampleContent.replaceAll('CATCODE', response.job.category_code);
                    sampleContent = sampleContent.replaceAll('CATNAME', response.job.category_name);
                    sampleContent = sampleContent.replaceAll('CATSUBCODE', response.job.subcategory_code);
                    sampleContent = sampleContent.replaceAll('CATSUBNAME', response.job.subcategory_name);
                    sampleContent = sampleContent.replaceAll('EMPCODE', response.job.employer_code);
                    sampleContent = sampleContent.replaceAll('EMPNAME', response.job.employer_name);
                    sampleContent = sampleContent.replaceAll('LOCCODE', response.job.location_code);
                    sampleContent = sampleContent.replaceAll('LOCNAME', response.job.location_name);
                    sampleContent = sampleContent.replaceAll('LOCNAME', response.job.location_name);
                    sampleContent = sampleContent.replaceAll('DETAILS', response.job.description);
                    sampleContent = sampleContent.replaceAll('SALARY', response.job.salary);
                    sampleContent = sampleContent.replaceAll('SHIFT', response.job.shift);
                    sampleContent = sampleContent.replaceAll('BENEFIT', response.job.benefit);
                    sampleContent = sampleContent.replaceAll('EXPIRE', response.job.expire);


                    $(".job-box").empty().html(sampleContent);

                    if(response.job.category_name == null)
                        $(".job-box .sec-cat").hide
                    if(response.job.subcategory_name == null)
                        $(".job-box .sec-subcat").hide();
                    if(response.job.employer_name == null)
                        $(".job-box .sec-emp").hide();
                    if(response.job.location_name == null)
                        $(".job-box .sec-loc").hide();
                    if(response.job.description == null)
                        $(".job-box .sec-details").hide();
                    if(response.job.salary == null)
                        $(".job-box .sec-salary").hide();
                    if(response.job.shift == null)
                        $(".job-box .sec-shift").hide();
                    if(response.job.benefit == null)
                        $(".job-box .sec-benefit").hide();
                    if(response.job.expire == null)
                        $(".job-box .sec-expire").hide();
                    if(response.job.is_saved)
                        $(".job-box .sec-saved").removeClass('btn-outline-primary').addClass('btn-primary');
                    switch(response.job.apply_code){
                        case 0:
                            $(".job-box .sec-apply").removeAttr('href').attr('disabled', true).removeClass('btn-primary').addClass('btn-dark').html('Inactive');
                            break;
                        case 1:
                            $(".job-box .sec-apply").html('Apply NOW');
                            break;
                        case 2:
                            $(".job-box .sec-apply").removeAttr('href', '#').attr('disabled', true).removeClass('btn-primary').addClass('btn-dark').html('Expired');
                            break;
                        default:
                            $(".job-box .sec-apply").hide();
                    }
                }
            });
        });

        $(".job-box").delegate('.fa-close', 'click', function(){
            $('.job-box').html('<div class="mt-5 text-center text-primary">Select any Job from left to view details or Apply.</div>')
        })

    </script>
@endpush
