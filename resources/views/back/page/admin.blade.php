@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row shapreter-row justify-content-center">
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
								<span><i class="fas fa-flag"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\Job::count() }}</h3>
                                <p class="mb-0">Total Job</spapn>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
								<span><i class="fas fa-check"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\Company::count() }}</h3>
                                <p class="mb-0">Total Employer</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
								<span><i class="fas fa-map-marker"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\Branch::count() }}</h3>
                                <p class="mb-0">Location</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
								<span><i class="fas fa-code-branch"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\Category::whereNull('parent_id')->count() }}</h3>
                                <p class="mb-0">Categories</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
								<span><i class="fas fa-paper-plane"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\CandidateJob::where('applied_at')->count() }}</h3>
                                <p class="mb-0">Applied Job</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6">
                            <div class="static-icon">
								<span><i class="fas fa-user"></i></span>
                                <h3 class="count mb-0">{{ \App\Models\User::where('type', 'candidate')->count() }}</h3>
                                <p class="mb-0">Job Seeker</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('style') @endpush
