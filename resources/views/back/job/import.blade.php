@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">{{ $title??'Import Job from CSV' }}</h3>
        <div>
            <a href="{{ route('panel.job.entity') }}" class="btn btn-primary btn-xs me-3"><i
                    class="fas fa-plus me-2"></i>Add New Job</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                {{ html()->form('POST', route('panel.job.import'))->acceptsFiles()->open() }}
                <div class="card-header text-center">
                    <h4 style="width: 100%">Upload your file</h4>
                </div>
                <div class="card-body text-center">
                    <p>Download the {{ html()->a(asset('Sample.csv'), 'Sample')->class('badge badge-info badge-xs')->attribute('download', 'Sample_Importer_'.\Carbon\Carbon::now()->timestamp.'.csv') }} file. Populate your data following the format.

                    <div class="row justify-content-center">
                        <div class="col-sm-12 col-md-10 col-lg-8">
                            {{ html()->label('Select CSV file', 'document')->class('form-label fw-bold fs-30') }}
                            {{ html()->file('document')->class('form-control')->accept('.csv')->required() }}
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    {{ html()->submit('Import Jobs')->class('btn btn-primary') }}
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header text-center">
                    <h4 style="width: 100%">Instruction</h4>
                </div>
                <div class="card-body">
                    <div class="custom-tab-1">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#prepare"> Prepare file</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#company"> Company</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#category"> Category</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="prepare" role="tabpanel">
                                <div class="pt-4">
                                    <p>To prepare file we recommend to download the sample file each time and fill up data.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="company">
                                <div class="pt-4">
                                    <table style="width: 100%; color: black">
                                        <thead>
                                        <tr class="text-start">
                                            <th class="border-bottom">Company</th>
                                            <th class="border-bottom">Location</th>
                                        </tr>
                                        </thead>
                                        <tbody class="text-start">
                                        @forelse($companies??[] as $company)
                                            <tr>
                                                <td colspan="2" class="text-start">{{ $company->name }}</td>
                                            </tr>
                                            @foreach($company->branches??[] as $branch)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $branch->location }}</td>
                                                </tr>
                                            @endforeach
                                        @empty
                                            <tr>
                                                <td colspan="100%">
                                                    <p>No company found</p>
                                                    {{ html()->a(route('panel.company.entity'), 'Add new Company')->class('btn btn-outline-primary btn-xs')->target('blank') }}
                                                </td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="category">
                                <div class="pt-4">
                                    <h5>All the listed categories and subcategories</h5>
                                    <ul>
                                        @forelse($categories??[] as $category)
                                            @if($category->children->count()??0)
                                                <li>
                                                    <p class="m-0">{{ $category->name }}</p>
                                                    <ul style="margin-left: 20px">
                                                        @foreach($category->children as $child)
                                                            <li>{{ $child->name }}</li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li>{{ $category->name }}</li>
                                            @endif
                                        @empty
                                            <p>No category found</p>
                                            {{ html()->a(route('panel.category.entity'), 'Add new Category')->class('btn btn-outline-primary btn-xs')->target('blank') }}
                                        @endforelse
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
