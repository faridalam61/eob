@extends('back.layout')

@push('style')
    <style>
        .job-card.selected {
            background-color: rgba(255, 182, 193, 0.1);
        }

        .job-card .job-header {
            border-bottom: 1px solid var(--primary);
        }

        .job-card .job-footer {
            border-top: 1px solid var(--primary);
        }
    </style>
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="d-block w-100">
                <div class="row">
                    <div class="col">
                        <h3 class="d-block w-100 p-0 m-0">{{ $job->title }}</h3>
                        <p class="w-100 p-0 m-0">{{ $job->subtitle }}</p>

                        <div class="d-block">
                            @if($job->category)
                                <a href="{{ route('panel.home', ['category' => $job->category->code]) }}">
                                    <i class="fa fa-code-branch"></i> {{ $job->category->name }}</a>
                            @endif
                            @if($job->subcategory)
                                <a href="{{ route('panel.home', ['category' => $job->subcategory->code]) }}">
                                    <i class="fa fa-circle-arrow-right"></i> {{ $job->subcategory->name }}
                                </a>
                            @endif
                        </div>
                        <div class="d-block py-1">
                            @if($job->company)
                                <a href="{{ route('panel.home', ['employer' => $job->company->code]) }}">
                                    <i class="fa fa-building"></i> Employer : {{ $job->company->name }}</a>
                            @endif
                            @if($job->branch)
                                <a href="{{ route('panel.home', ['location' => $job->branch->code]) }}">
                                    <i class="fa fa-map-marker"></i> Location : {{ $job->branch->location }}</a>
                            @endif
                        </div>
                        @if($job->expire_at)
                            <div class="d-block">
                                Open for application till : {{ \Carbon\Carbon::parse($job->expire_at)->format('d M Y') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-auto text-end">
                        @if($job->active == 2)
                            <span class="btn btn-dark btn-sm mb-2"><i class="fa fa-clock"></i> Pending Decision</span>
                        @elseif($job->active == 0)
                            <span class="btn btn-dark btn-sm mb-2"><i class="fa fa-close"></i> Closed</span>
                        @elseif(!null == $job->expire_at && \Carbon\Carbon::parse($job->expire_at)->isBefore(\Carbon\Carbon::now()))
                            <span class="btn btn-dark btn-sm mb-2"><i class="fa fa-stop-circle"></i> Expired </span>
                        @else
                            <span class="btn btn-primary btn-sm mb-2"><i class="fa fa-paper-plane"></i> Apply Now</span>
                        @endif
                        <br>
                        <button
                            class="btn btn-sm save-now {{ $job->is_saved?'btn-primary':'btn-outline-primary' }}"
                            code="{{ $job->code }}" style="margin-left: 10px"><i
                                class="fa-solid fa-save"></i> {{ $job->is_saved?'Saved':'Save now' }}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body" style="min-height: 90vh">

            <div class="d-block p-3">
                <h3>Job Details</h3>
                <p>{!! $job->description !!}</p>
                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Salary</h3>
                <p>{{ $job->salary_estimate }}</p>
                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Shift</h3>
                <p>{{ $job->shift }}</p>
                <h3 class="mt-3"><i class="fa-solid fa-money"></i> Benefit</h3>
                <p>{{ $job->benefit }}</p>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script>
        $(".save-now").click(function (e) {
            var url = "{{ route('ajax.job.save', 'CODE') }}";
            var button = $(this).closest('button');
            url = url.replace('CODE', $(this).attr('code'));
            $.ajax({
                url: url.replace('CODE', $(this).attr('code')), success: function (result) {
                    var replay = $.parseJSON(result);
                    console.log(replay.saved);
                    if (!null == replay.saved) {
                        button.removeClass('btn-outline-primary').addClass('btn-primary');
                    } else {
                        button.addClass('btn-outline-primary').removeClass('btn-primary');
                    }
                }
            });
            e.preventDefault();
        });
    </script>
@endpush
