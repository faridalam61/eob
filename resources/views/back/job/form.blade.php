@php($guard = auth()->user()->type)

@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">
        <h3 class="mb-0 me-auto">{{ $title }}</h3>
    </div>
    <div class="card">
        @if($job??false)
            {{ html()->form('POST', route('panel.job.entity', $job->code))->open() }}
        @else
            {{ html()->form('POST', route('panel.job.entity'))->open() }}
        @endif

        <div class="card-header d-none">
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    {{ html()->label('Title', 'title')->class('form-label') }}
                    {{ html()->text('title', $job->title??null)->class('form-control')->placeholder('Job title')->required()->autofocus() }}
                </div>
                <div class="col-sm-6 mb-3">
                    {{ html()->label('SubTitle', 'subtitle')->class('form-label') }}
                    {{ html()->text('subtitle', $job->subtitle??null)->class('form-control')->placeholder('Sub-title')->required() }}
                </div>
                <div class="col-sm-6 mb-3">
                    @if($guard == 'employer')
                        {{ html()->hidden('company_id', $companies->first()->id??null) }}
                    @elseif(isset($companies) && $companies->count()??0)
                        {{ html()->label('Company', 'company_id')->class('form-label') }}
                        {{ html()->select('company_id', $companies->pluck('name', 'id')??null, $job->company_id??null)->class('form-control mb-3')->placeholder('Select company')->required() }}
                    @endif

                    {{ html()->label('Job Location', 'branch_id')->class('form-label') }}
                    {{ html()->select('branch_id', isset($branches)?$branches->pluck('location', 'id'):[], $job->branch_id??null)->class('form-control mb-3')->placeholder('Select location')->required() }}

                    {{ html()->label('Category', 'category_id')->class('form-label') }}
                    @if($categories??null)
                        {{ html()->select('category_id', $categories->pluck('name', 'id')??null, $job->category_id??null)->class('form-control mb-3')->placeholder('Select Category')->required() }}
                    @else
                        {{ html()->text('category_id', $job->category->name??null)->class('form-control mb-3')->placeholder('Category name')->required() }}
                    @endif

                    {{ html()->label('Sub Category', 'subcategory_id')->class('form-label') }}
                    {{ html()->select('subcategory_id', isset($subcategories)?$subcategories->pluck('name', 'id'):null, $job->subcategory_id??null)->class('form-control')->placeholder('Sub category ')->required() }}
                </div>
                <div class="col-sm-6 mb-3">
                    @if($guard != 'employer')
                        <div class="input-company-url">
                            {{ html()->label('Company Url', 'company_url')->class('form-label') }}
                            {{ html()->text('company_url', $job->company->info_url??null)->class('form-control mb-3')->placeholder('Company website')->required() }}
                        </div>
                    @endif

                    {{ html()->label('Job Url', 'job_url')->class('form-label') }}
                    {{ html()->text('job_url', $job->info_url??null)->class('form-control mb-3')->placeholder('Job url')->required() }}

                    <div class="row">
                        <div class="col-sm-6">
                            {{ html()->label('Job Type', 'job_type')->class('form-label') }}
                            {{ html()->text('job_type', $job->type??null)->class('form-control mb-3')->placeholder('Job type')->required() }}

                            {{ html()->label('Salary Estimate', 'salary_estimate')->class('form-label') }}
                            {{ html()->text('salary_estimate', $job->salary_estimate??null)->class('form-control')->placeholder('Estimated salary')->required() }}
                        </div>
                        <div class="col-sm-6">
                            {{ html()->label('Shift', 'shift')->class('form-label') }}
                            {{ html()->text('shift', $job->shift??null)->class('form-control mb-3')->placeholder('Shift')->required() }}

                            {{ html()->label('Benefits', 'benefits')->class('form-label') }}
                            {{ html()->text('benefits', $job->benefit??null)->class('form-control')->placeholder('Benefits')->required() }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    {{ html()->label('Details', 'details')->class('form-label') }}
                    {{ html()->textarea('details', $job->description??null)->class('form-control mb-3')->rows(4)->placeholder('Job details')->required() }}
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-sm-6">
                    {{ html()->reset('Reset')->class('btn btn-dark btn-sm') }}
                </div>
                <div class="col-sm-6">
                    {{ html()->submit('Submit')->class('btn btn-primary btn-sm') }}
                </div>
            </div>
        </div>
        {{ html()->form() }}
    </div>

@endsection

@push('script')
    <script>
        $('#company_id').change(function () {
            var url = "{{ route('ajax.branches', 'CODE') }}";
            url = url.replace('CODE', $(this).val());
            var selector = $('#branch_id');
            $.ajax({
                url: url, success: function (result) {
                    var replay = $.parseJSON(result);
                    console.log(replay);
                    var options = '<option value="">Select  subcategory</option>';
                    $.each(replay, function (key, value) {
                        options += '<option value="' + key + '">' + value + '</option>';
                    });
                    selector.html(options);
                }
            });
        });

        $('#category_id').change(function () {
            var url = "{{ route('ajax.subscategories', 'CODE') }}";
            url = url.replace('CODE', $(this).val());
            var selector = $('#subcategory_id');
            $.ajax({
                url: url, success: function (result) {
                    var replay = $.parseJSON(result);
                    console.log(replay);
                    var options = '<option value="">Select any subcategory</option>';
                    $.each(replay, function(key, value){
                        options += '<option value="'+key+'">'+value+'</option>';
                    });
                    selector.html(options);
                }
            });
        });

        $("#company_id, #branch_id, #category_id, #subcategory_id").select2();
    </script>
@endpush
