@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4 flex-wrap">
        <h3 class="me-auto">Job List</h3>
        @if(in_array(auth()->user()->type, ['admin', 'employer']))
            <div>
                <a href="{{ route('panel.job.entity') }}" class="btn btn-primary btn-xs me-3">
                    <i class="fas fa-plus me-2"></i>Add New Job</a>
                <a href="{{ route('panel.job.import') }}" class="btn btn-primary btn-xs me-3">
                    <i class="fas fa-file-lines me-2"></i>Import Job</a>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="table-responsive">
                <table class="table display mb-4 dataTablesCard job-table table-responsive-xl card-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Job Title</th>
                        <th>Position</th>
                        <th>Type</th>
                        <th>Date Posted</th>
                        <th>Last Date To Apply</th>
                        <th>Company</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x = $jobs->firstItem())
                    @forelse($jobs as $job)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ $job->title }}</td>
                            <td>{{ $job->subtitle }}</td>
                            <td class="wspace-no">{{ $job->type }}</td>
                            <td>{{ \Carbon\Carbon::parse($job->post_date)->format('Y M d') }}</td>
                            <td>{{ \Carbon\Carbon::parse($job->expire_at)->format('Y M d') }}</td>
                            <td>{{ $job->company->name??'-- ? --' }}
                                <br><small>{{ $job->branch->location??'-- ? --' }}</small></td>
                            <td>
                                @if($job->active == 2)
                                    <span class="badge badge-warning badge-xs light text-dark">Pending Decision</span>
                                @elseif($job->active == 0)
                                    <span class="badge badge-warning badge-xs light text-dark">Closed</span>
                                @elseif(!null == $job->expire_at && \Carbon\Carbon::parse($job->expire_at)->isBefore(\Carbon\Carbon::now()))
                                    <span class="badge badge-danger badge-xs light text-dark"> Updated </span>
                                @else
                                    <span class="badge badge-success badge-xs light text-dark">Open</span>
                                @endif
                            </td>
                            <td>
                                <div class="action-buttons d-flex justify-content-end">
                                    <a href="{{ route('panel.job.show', $job->code) }}"
                                       class="btn btn-success btn-sm light mr-2" title="Details">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    @if(in_array(auth()->user()->type, ['admin', 'employer']))
                                        <a href="{{ route('panel.job.entity', $job->code) }}"
                                           class="btn btn-secondary btn-sm light mr-2" title="Modify"><i
                                                class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-sm light" title="Delete" data-bs-toggle="modal"
                                           data-bs-target="#delete{{ $job->code }}"><i class="fa fa-trash"></i> </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <div class="modal fade" id="delete{{ $job->code }}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {{ html()->form('delete', route('panel.job.delete', $job->code))->open() }}
                                    <div class="modal-header">
                                        <h5 class="modal-title">Delete confirmation</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal">
                                        </button>
                                    </div>
                                    <div class="modal-body">Do you want to delete  <i
                                            class="text-info">{{ $job->title }}</i> permanently ? 
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row" style="display: contents">
                                            <div class="col">
                                                {{ html()->button('Close')->class('btn btn-info btn-sm float-start')->attribute('data-bs-dismiss', 'modal')->type('button') }}
                                            </div>
                                            <div class="col text-end">
                                                {{ html()->submit('Delete')->class('btn btn-primary btn-sm') }}
                                            </div>
                                        </div>
                                    </div>
                                    {{ html()->form()->close() }}
                                </div>
                            </div>
                        </div>
                    @empty
                        <tr>
                            <td colspan="100%" class="text-center">
                                <p>Entries not found</p>
                                @if(in_array(auth()->user()->type, ['admin', 'employer']))
                                    To add to this list go
                                    to {{ html()->a(route('panel.job.entity'), 'Add new job')->class('badge badge-primary badge-sm') }}
                                    or {{ html()->a(route('panel.job.import'), 'Import form CSV')->class('badge badge-primary badge-sm') }}
                                    .
                                @endif
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr class="text-center">
                        <td colspan="100%">
                            {{ $jobs->withQueryString()->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
