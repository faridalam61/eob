@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="card">
                {{ html()->form()->open() }}
                <div class="card-header">
                    <h4 class="mb-0 me-auto text-center w-100">{{ $title }}</h4>
                </div>

                <div class="card-body">

                    <h5 class="text-center">Select your job category preference </h5>

                    @foreach($branches??[] as $branch)
                        <div class="form-check custom-checkbox ms-1">
                            {{ html()->hidden('branches['.$branch->id.']', false) }}
                            {{ html()->checkbox('branches['.$branch->id.']', in_array($branch->id, $myBranchIds)?true:false, true)->id('opp'.$branch->code)->class('form-check-input') }}
                            {{ html()->label($branch->location, 'opp'.$branch->code)->class('form-check-label') }}
                        </div>
                    @endforeach

                </div>
                <div class="card-footer text-center">
                    <div class="row">
                        <div class="col">
                            {{ html()->a(route('panel.profile'), 'Back')->class('btn btn-dark btn-sm') }}
                        </div>
                        <div class="col">
                            {{ html()->submit('Update')->class('btn btn-primary btn-sm') }}
                        </div>
                    </div>
                </div>

                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
