@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="card">

                {{ html()->form('POST', route('panel.profile.modify'))->open() }}

                <div class="card-header">
                    <h4 class="mb-0 me-auto text-center w-100">{{ $title }}</h4>
                </div>

                <div class="card-body">
                    <div class="mb-3">
                        {{ html()->label('Password <small class="float-end">Minimum 6 character</small>', 'password')->class('form-label w-100') }}
                        {{ html()->password('password')->class('form-control')->placeholder('Enter New password')->required()->autofocus() }}
                    </div>
                    <div class="mb-3">
                        {{ html()->label('Confirm Password', 'password_confirmation')->class('form-label') }}
                        {{ html()->password('password_confirmation')->class('form-control')->placeholder('Confirm Passowrd')->required() }}
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            {{ html()->reset('Reset')->class('btn btn-dark btn-sm') }}
                        </div>
                        <div class="col-sm-6">
                            {{ html()->submit('Update')->class('btn btn-primary btn-sm') }}
                        </div>
                    </div>
                </div>
                {{ html()->form() }}
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
