@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 me-auto text-center w-100">{{ $title }}</h4>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <tr>
                            <th>First Name</th>
                            <td width="1%" style="text-wrap: nowrap;">:</td>
                            <td>{{ $user->first_name??'' }}</td>
                        </tr>
                        <tr>
                            <th>Last Name</th>
                            <td>:</td>
                            <td>{{ $user->last_name??'' }}</td>
                        </tr>
                        <tr>
                            <th>Email Address</th>
                            <td>:</td>
                            <td>{{ $user->email??'' }}</td>
                        </tr>
                        <tr>
                            <th>Phone Number</th>
                            <td>:</td>
                            <td>{{ $user->phone??'' }}</td>
                        </tr>
                    </table>

                    @if($user->type == 'employer')
                        @php($company = $user->company)
                        <h5 class="text-center">Company Information </h5>
                        <table class="table table-striped">
                            <tr>
                                <th>Company Name</th>
                                <td width="1%" style="text-wrap: nowrap;">:</td>
                                <td>{{ $company->name??'' }}</td>
                            </tr>
                            <tr>
                                <th>Email Address</th>
                                <td>:</td>
                                <td>{{ $company->email??'' }}</td>
                            </tr>
                            <tr>
                                <th>Phone Number</th>
                                <td>:</td>
                                <td>{{ $company->phone??'' }}</td>
                            </tr>
                            <tr>
                                <th>Website</th>
                                <td>:</td>
                                <td>{{ parse_url($company->info_url)['host']??'' }}</td>
                            </tr>
                            <tr>
                                <th>HR Contact</th>
                                <td>:</td>
                                <td>{{ $company->hr_contact??'' }}</td>
                            </tr>
                        </table>
                        @endif

                    @if($user->type == 'employer')
                        <h5 class="text-center">
                            Locations under you
                            <a href="{{ route('panel.profile.branch') }}" title="Edit choice">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </h5>
                    <div class="text-center">
                        <ol class="">
                            @forelse($user->branches??[] as $branch)
                                <li>{{ $branch->location }}</li>
                            @empty
                                <p class="text-center"> No branch selected </p>
                            @endforelse
                        </ol>
                    </div>

                    @elseif($user->type == 'candidate')
                        <h5 class="text-center">
                            My Job Preference
                            <a href="{{ route('panel.jop-preference') }}" title="Edit choice">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </h5>
                        <div class="text-center">
                            <div class="row">
                                <div class="col">
                                    <h5>Preferred Categories</h5>
                                    <ul>
                                        @forelse($user->categories??[] as $category)
                                            <li>{{ $category->name }}</li>
                                        @empty
                                            <p class="text-center"> No category selected </p>
                                        @endforelse
                                    </ul>
                                </div>
                                <div class="col">
                                    <h5>Preferred Types</h5>
                                    <ul>
                                        @if(!null == $user->job_types)
                                            @foreach(explode('|', $user->job_types)??[] as $type)
                                                <li>{{ isset($jobTypes)?($jobTypes[$type]??$type):$type }}</li>
                                            @endforeach
                                        @else
                                            <p>No Type</p>
                                        @endif
                                    </ul>

                                </div>
                            </div>

                        </div>
                    @endif
                    <hr>
                    <h5 class="text-center">Link account to Login with</h5>
                    <div class="text-center">
                        <table class="table text-center">
                            <tr>
                                <td>
                                    @if($user->google_uuid)
                                        <span class="btn btn-rounded btn-google-plus"><span
                                                class="btn-icon-start text-primary">G
                                    </span>Google Already Linked</span>
                                    @else
                                        <a href="{{ route('auth', 'google') }}" type="button"
                                           class="btn btn-rounded btn-google-plus"><span
                                                class="btn-icon-start text-primary">G
                                    </span>Link Google account</a>
                                    @endif
                                </td>
                                <td>
                                    @if($user->facebook_uuid)
                                        <span class="btn btn-rounded btn-facebook"><span
                                                class="btn-icon-start text-primary">f
                                    </span>Facebook Already Linked</span>
                                    @else
                                        <a href="{{ route('auth', 'facebook') }}" type="button"
                                           class="btn btn-rounded btn-facebook"><span
                                                class="btn-icon-start text-primary">f
                                    </span>Link Facebook account</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <div class="card-footer text-center">
                    {{ html()->a(route('panel.profile.modify'), 'Update Profile')->class('btn btn-primary btn-xs') }}
                    {{ html()->a(route('panel.profile.change-password'), 'Change Password')->class('btn btn-primary btn-xs') }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
