@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="card">
                {{ html()->form()->open() }}
                <div class="card-header">
                    <h4 class="mb-0 me-auto text-center w-100">{{ $title }}</h4>
                </div>

                <div class="card-body">

                    <h5 class="text-center">Select your job category preference </h5>
                        <div class="mb-3">
                            {{ html()->label('Looking for job type', 'job_type_selector')->class('form-label') }}
                            {{ html()->select('job_types[]', $types??[], isset($user->job_types)?explode('|', $user->job_types):null)->multiple()->class('form-control multi-select')->id('job_type_selector')->required() }}
                        </div>

                    <h5 class="text-center">Select your job category preference </h5>

                    @foreach($categories??[] as $category)
                        <div class="form-check custom-checkbox ms-1">
                            {{ html()->checkbox('categories[]', in_array($category->id, $myCategories)?true:false, $category->code)->id($category->code)->class('form-check-input') }}
                            {{ html()->label($category->name, $category->code)->class('form-check-label') }}
                        </div>
                    @endforeach

                </div>
                <div class="card-footer text-center">
                    <div class="row">
                        <div class="col">
                            {{ html()->a(route('panel.profile'), 'Back')->class('btn btn-dark btn-sm') }}
                        </div>
                        <div class="col">
                            {{ html()->submit('Update')->class('btn btn-primary btn-sm') }}
                        </div>
                    </div>
                </div>

                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
    $("#job_type_selector").select2();
</script>
@endpush
