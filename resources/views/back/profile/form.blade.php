@extends('back.layout')

@push('style') @endpush

@section('content')
    <div class="d-flex align-items-center mb-4">
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="card">

                    {{ html()->form('POST', route('panel.profile.modify'))->open() }}

                <div class="card-header">
                    <h4 class="mb-0 me-auto text-center w-100">{{ $title }}</h4>
                </div>


                <div class="card-body">

                        <div class="mb-3">
                            {{ html()->label('First Name', 'first_name')->class('form-label') }}
                            {{ html()->text('first_name', $user->first_name??null)->class('form-control')->placeholder('Enter first name')->required()->autofocus() }}
                        </div>

                        <div class="mb-3">
                            {{ html()->label('Last Name', 'last_name')->class('form-label') }}
                            {{ html()->text('last_name', $user->last_name??null)->class('form-control')->placeholder('Enter last name')->required() }}
                        </div>

                        <div class="mb-3">
                            {{ html()->label('Email Address', 'email')->class('form-label') }}
                            {{ html()->email('email', $user->email??null)->class('form-control')->placeholder('Enter email address')->required() }}
                        </div>

                        <div class="mb-3">
                            {{ html()->label('Phone number', 'phone')->class('form-label') }}
                            {{ html()->text('phone', $user->phone??null)->class('form-control')->attribute('type', 'tel')->placeholder('Enter phone number')->required() }}
                        </div>


                    @if(auth()->user()->type == 'employer')
                        <hr>
                        <div class="mb-3">
                            {{ html()->label('Company Name', 'company[name]')->class('form-label') }}
                            {{ html()->text('company[name]', $user->company->name??null)->class('form-control')->placeholder('Enter company name')->required() }}
                        </div>

                        <div class="mb-3">
                            {{ html()->label('Company Email', 'company[email]')->class('form-label') }}
                            {{ html()->email('company[email]', $user->company->email??null)->class('form-control')->placeholder('Company email')->required() }}
                        </div>

                        <div class="mb-3">
                            {{ html()->label('Company Phone Number', 'company[phone]')->class('form-label') }}
                            {{ html()->text('company[phone]', $user->company->phone??null)->class('form-control')->placeholder('Enter phone of company')->required() }}
                        </div>

                        <div class="mb-3">
                            {{ html()->label('Company Website Url', 'company[website]')->class('form-label') }}
                            {{ html()->text('company[website]', $user->company->info_url??null)->class('form-control')->attribute('type', 'url')->placeholder('Company Website')->required() }}
                        </div>

                        <div class="mb-3">
                            {{ html()->label('HR Contact number', 'company[hr_contact]')->class('form-label') }}
                            {{ html()->text('company[hr_contact]', $user->company->hr_contact??null)->class('form-control')->attribute('type', 'tel')->placeholder('Enter first name')->required() }}
                        </div>
                    @endif

                </div>
                <div class="card-footer">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            {{ html()->reset('Reset')->class('btn btn-dark btn-sm') }}
                        </div>
                        <div class="col-sm-6">
                            {{ html()->submit('Update')->class('btn btn-primary btn-sm') }}
                        </div>
                    </div>
                </div>
                {{ html()->form() }}
            </div>
        </div>
    </div>
@endsection

@push('script') @endpush
