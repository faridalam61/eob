{{---	All users--}}
{{---	All employers--}}
{{---	All job listings--}}
{{---	Settings--}}


@if(in_array($guard, ['admin', 'employee']))

@endif

<li>
    <a class="" href="{{ route('panel.home') }}" aria-expanded="false">
        @if($guard !== 'candidate')
            <i class="flaticon-025-dashboard"></i>
            <span class="nav-text"> Dashboard</span>
        @else
            <i class="fa-solid fa-search"></i>
            <span class="nav-text">Job Search</span>
        @endif
    </a>
</li>


@if(in_array($guard, ['admin', 'employer', 'candidate']))
    <li><a class="has-arrow " href="javascript:void()" aria-expanded="false">
            <i class="flaticon-093-waving"></i>
            <span class="nav-text">{{ $guard == 'admin'?'':'My ' }}Jobs</span>
        </a>
        <ul aria-expanded="false">
            @if(in_array($guard, ['admin', 'employer']))
                <li><a href="{{ route('panel.job.entity') }}">Add New Job</a></li>
            @endif
            @if($guard == 'admin')
                <li><a href="{{ route('panel.job.import') }}">Import Job</a></li>
                <li><a href="{{ route('panel.category.list') }}">Job Categories</a></li>
            @endif
            @if($guard == 'candidate')
                <li><a href="{{ route('panel.job.saved') }}">Saved Jobs</a></li>
                <li><a href="{{ route('panel.job.applied') }}">Applied Jobs</a></li>
            @endif
            @if($guard != 'candidate')
                <li><a href="{{ route('panel.job.list') }}">All Job List</a></li>
            @endif
        </ul>
    </li>
@endif

@if(in_array($guard, ['admin']))
    <li><a class="has-arrow " href="javascript:void()" aria-expanded="false">
            <i class="flaticon-013-checkmark"></i>
            <span class="nav-text">Employer</span>
        </a>
        <ul aria-expanded="false">
            <li><a href="{{ route('panel.company.entity') }}">Add Employer</a></li>
            <li><a href="{{ route('panel.company.list') }}">Employer List</a></li>
            <li><a href="{{ route('panel.company.invite') }}">Employer Invitation</a></li>
        </ul>
    </li>
@endif

@if(in_array($guard, ['admin']))
    <li><a class="has-arrow " href="javascript:void()" aria-expanded="false">
            <i class="fa-solid fa-user"></i>
            <span class="nav-text">Job Seekers</span>
        </a>
        <ul aria-expanded="false">
            <li><a href="{{ route('panel.candidate.entity') }}">Add New</a></li>
            <li><a href="{{ route('panel.candidate.list') }}">Users List</a></li>
        </ul>
    </li>
@endif



<li><a class="has-arrow " href="javascript:void()" aria-expanded="false">
        <i class="fa-solid fa-gear"></i>
        <span class="nav-text">Setting</span>
    </a>
    <ul aria-expanded="false">
        @if(in_array($guard, ['admin']))
            <li><a href="{{ route('panel.setting') }}">System</a></li>
        @endif
        <li><a href="{{ route('panel.profile.modify') }}">Update Profile</a></li>
        <li><a href="{{ route('panel.profile.change-password') }}">Change Password</a></li>
    </ul>
</li>

